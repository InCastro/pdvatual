import React, { PropsWithChildren, Reducer, createContext, useState, useContext, useReducer } from 'react'

interface IVendas {
  ID_VENDA: number,
  ID_TURNO: number,
  COD_AV: number,
  COD_OPERADOR: string,
  COD_VENDEDOR: string,
  ITENS: Array<IVendas_Itens>,
  PAGAMENTO: Array<IVendas_Pagamento>,
  QUANTIDADE_TOTAL: number,
  VALOR_ITENS_AV: number,
  VALOR_ITENS_PL: number,
  VALOR_DESCONTO_PL: number,
  VALOR_DESCONTO_AV: number,
  VALOR_FRETE: number,
  VALOR_VENDA_AV: number,
  VALOR_VENDA_PL: number,
  VALOR_TOTAL: number
}

interface IVendas_Itens {
  DESCRICAO: string,
  SKU: string,
  EAN: string,
  KEY_RESERVAPROD: string,
  QUANTIDADE: number,
  VALOR_UNITARIO: number,
  VALOR_FRETE: number,
  DESCONTO: number,
  DESCONTO_RATEIO: number,
  VALOR_ITEM: number,
  ITEM_AV: boolean,
  INDEX: number,
  CHECKED: boolean
}

interface IVendas_Pagamento {
  COD_PAGAMENTO: number,
  DESCRICAO_PAGAMENTO: string,
  VALOR_PAGAMENTO: number,
  NSU_PAGAMENTO: string,
  NUM_PARCELA: number
}


const initialStateVenda: IVendas = {
  ID_VENDA: 0,
  ID_TURNO: 0,
  COD_AV: 0,
  COD_OPERADOR: '',
  COD_VENDEDOR: '',
  ITENS: [],
  PAGAMENTO: [],
  QUANTIDADE_TOTAL: 0,
  VALOR_ITENS_AV: 0,
  VALOR_ITENS_PL: 0,
  VALOR_DESCONTO_PL: 0,
  VALOR_DESCONTO_AV: 0,
  VALOR_FRETE: 0,
  VALOR_VENDA_AV: 0,
  VALOR_VENDA_PL: 0,
  VALOR_TOTAL: 0
}

const initialStateItens: IVendas_Itens = {
  DESCRICAO: '',
  SKU: '',
  EAN: '',
  QUANTIDADE: 0,
  KEY_RESERVAPROD: '',
  VALOR_UNITARIO: 0,
  VALOR_FRETE: 0,
  DESCONTO: 0,
  DESCONTO_RATEIO: 0,
  VALOR_ITEM: 0,
  ITEM_AV: false,
  INDEX: 0,
  CHECKED: false
}

type IConfigPDV = {
  RAZAO_SOCIAL: string,
  FANTASIA: string,
  CNPJ_EMPRESA: string,
  IE: string,
  ENDERECO_EMPRESA: string,
  CEP_EMPRESA: string,
  FONE_EMPRESA: string,
  NUM_CAIXA: string,
  VALOR_SANGRIA: number,
  REGIME_TRIBUTARIO: string,
  COD_ATVSAT: string,
  ASS_SAT: string,
  AMBIENTE_SAT: string
}

let initialDadosConfig: IConfigPDV =
{
  RAZAO_SOCIAL: '',
  FANTASIA: '',
  CNPJ_EMPRESA: '',
  IE: '',
  ENDERECO_EMPRESA: '',
  CEP_EMPRESA: '',
  FONE_EMPRESA: '',
  NUM_CAIXA: '',
  VALOR_SANGRIA: 0,
  REGIME_TRIBUTARIO: '0',
  COD_ATVSAT: '',
  ASS_SAT: '',
  AMBIENTE_SAT: '0'
}

const initalState = {
  cabecalho: {
    ITENS: [],
    INDEX_LISTORC: 0,
    VALOR_TOTAL: 0,
    TOTAL_QUANTIDADE: 0,
    TOTAL_ITEM: 0,
    TOTAL_FRETE: 0,
  },
  item: {
    DESCRICAO: '',
    SKU: '',
    EAN: '',
    QUANTIDADE: 0,
    VALOR_UNITARIO: 0,
    VALOR_FRETE: 0,
    DESCONTO: 0,
    DESCONTO_RATEIO: 0,
    ITEM_AV: false,
    INDEX: 0
  },
}

type IVenda = {
  ID_VENDA: number,
  ID_TURNO: number,
  COD_AV: number,
  COD_OPERADOR: string,
  COD_VENDEDOR: string,
  ITENS: Array<IItensVenda>,
  QUANTIDADE_TOTAL: number,
  VALOR_ITENS_AV: number,
  VALOR_ITENS_PL: number,
  VALOR_DESCONTO_PL: number,
  VALOR_DESCONTO_AV: number,
  VALOR_FRETE: number,
  VALOR_VENDA_AV: number,
  VALOR_VENDA_PL: number,
  VALOR_TOTAL: number
}

type IItensVenda = {
  DESCRICAO: string,
  SKU: string,
  EAN: string,
  KEY_RESERVAPROD: string,
  QUANTIDADE: number,
  VALOR_UNITARIO: number,
  VALOR_FRETE: number,
  DESCONTO: number,
  DESCONTO_RATEIO: number,
  VALOR_ITEM: number,
  ITEM_AV: boolean,
  INDEX: number,
  CHECKED: boolean
}

const initialStateVendaSuspensa: IVenda = {
  ID_VENDA: 0,
  ID_TURNO: 0,
  COD_AV: 0,
  COD_OPERADOR: '',
  COD_VENDEDOR: '',
  ITENS: [],
  QUANTIDADE_TOTAL: 0,
  VALOR_ITENS_AV: 0,
  VALOR_ITENS_PL: 0,
  VALOR_DESCONTO_PL: 0,
  VALOR_DESCONTO_AV: 0,
  VALOR_FRETE: 0,
  VALOR_VENDA_AV: 0,
  VALOR_VENDA_PL: 0,
  VALOR_TOTAL: 0
}

type IOrcItens = {
  DESCRICAO: string
  SKU: string
  EAN: ''
  QUANTIDADE: number
  VALOR_UNITARIO: number
  VALOR_FRETE: number
  DESCONTO: number
  DESCONTO_RATEIO: number
  ITEM_AV: true
}

type IOrcCabecalho = {
  ID_ORCAMENTO: number
  CLIENTE_NOME: string
  ITENS: Array<IOrcItens>
  INDEX_LISTORC: number
  TOTAL_QUANTIDADE: number
  TOTAL_ITEM: number
  TOTAL_FRETE: number
  VALOR_TOTAL: number
}

interface TypesContext {
  G_Venda: IVendas
  SetGVenda: (data: IVendas) => void

  G_DadosConfigPdv: IConfigPDV
  SetGDadosPdv: (data: any) => void

  G_PosicaoProdTable: number
  SetPosicaoProdTable: (data: number) => void

  G_PosicaoProdTableClick: number
  SetPosicaoProdTableClick: (data: number) => void

  ListVenda: Array<IVenda>
  SetListVenda: (data: []) => void

  G_ListOrc: Array<IOrcCabecalho>
  SetListOrc: (data: []) => void

  G_CpfCnpj: string
  SetCpfCnpj: (data: string) => void
  G_ValidateCpfCnpj: boolean
  SetValidateCpf: (data: boolean) => void

  FormaDePagamento: number
  SetFormaDePagamento: (data: number) => void

  G_DescMontagemCarrinho: boolean
  SetDescMontagemCarrinho: (data: boolean) => void

  G_DescFinalizarVenda: boolean
  SetDescFinalizarVenda: (data: boolean) => void

  SelectedItem: IVendas_Itens
  SetSelectedItem: (data: any) => void

  G_CheckedItem: number[]
  SetCheckedItem: (data: number[]) => void

  Resta: number
  SetResta: (data: number) => void

  PdvIsOpen: boolean
  SetPdvIsOpen: (data: boolean) => void
  OpenDialogMenuSat: boolean
  SetOpenDialogMenuSat: (data: boolean) => void

  OpenDialogSangria: boolean
  SetOpenDialogSangria: (data: boolean) => void
  TipoSangriaSup: string
  G_ValorSanSup: string;
  SetValorSanSup: (data: string) => void;

  G_ValorDesc: string;
  SetValorDesc: (data: string) => void;

  G_PercentualDesc: string;
  SetPercentualDesc: (data: string) => void;

  OpenDialogCPF: boolean
  SetOpenDialogCPF: (data: boolean) => void

  Loading: boolean
  SetLoading: (data: boolean) => void

  OpenDialogDesconto: boolean
  SetOpenDialogDesconto: (data: boolean) => void

  OpenMenuPdv: boolean
  SetMenuPdv: (data: boolean) => void

  Pagamentos: any
  SetPagamentos: (data: any) => void

  OpenListaOrc: boolean
  SetOpenListaOrc: (data: boolean) => void

  OpenFormasDePagamentos: boolean
  SetOpenFormasDePagamentos: (data: boolean) => void

  G_StatusVendaAv: boolean
  SetStatusVendasAv: (data: boolean) => void

  G_StatusVendaPl: boolean
  SetStatusVendasPl: (data: boolean) => void

  G_DialogQuestion: boolean
  SetDialogQuestion: (data: boolean) => void

  G_DialogWarning: boolean
  SetDialogWarning: (data: boolean) => void

  G_DialogError: boolean
  SetDialogError: (data: boolean) => void

  OpenDialogEncerramento: boolean
  SetOpenDialogEncerramento: (data: boolean) => void

  G_ConfigInitial: boolean
  SetConfigInitial: (data: boolean) => void

  G_OpenListaVendaSuspensa: boolean
  SetOpenListaVendaSuspensa: (data: boolean) => void

  G_StatusCaixa: string;
  SetGStatusCaixa: (data: string) => void

  G_IdTurno: number;
  SetGIdTurno: (data: number) => void

  LoggedIn: boolean
  SetLoggedIn: (data: boolean) => void

  G_TotalIteChecked: number;
  SetGTotalIteChecked: (data: number) => void
}

const ValueInitial: TypesContext = {
  G_Venda: initialStateVenda,
  SetGVenda: (data) => initialStateVenda,

  G_DadosConfigPdv: initialDadosConfig,
  SetGDadosPdv: (data) => { },

  ListVenda: [],
  SetListVenda: (data) => { },

  G_PosicaoProdTable: -1,
  SetPosicaoProdTable: (data: number) => -1,

  G_PosicaoProdTableClick: -1,
  SetPosicaoProdTableClick: (data: number) => -1,

  G_ListOrc: [],
  SetListOrc: (data) => { },

  G_CpfCnpj: '',
  SetCpfCnpj: (data) => '',

  G_DescMontagemCarrinho: false,
  SetDescMontagemCarrinho: (data) => false,

  G_DescFinalizarVenda: false,
  SetDescFinalizarVenda: (data) => false,


  G_ValidateCpfCnpj: false,
  SetValidateCpf: (data) => false,

  FormaDePagamento: 1,
  SetFormaDePagamento: (data) => 1,

  SelectedItem: initialStateItens,

  SetSelectedItem: (data) => '',

  G_CheckedItem: [],
  SetCheckedItem: (data) => { },

  Pagamentos: [],
  SetPagamentos: (data) => { },

  Resta: 0,
  SetResta: (data) => 0,

  OpenDialogMenuSat: false,
  SetOpenDialogMenuSat: (data) => false,

  OpenDialogSangria: false,
  SetOpenDialogSangria: (data) => false,
  TipoSangriaSup: '',
  G_ValorSanSup: '',
  SetValorSanSup: (data: string) => '',

  G_ValorDesc: '',
  SetValorDesc: (data: string) => '',

  G_PercentualDesc: '',
  SetPercentualDesc: (data: string) => '',

  OpenDialogCPF: false,
  SetOpenDialogCPF: (data) => false,

  Loading: true,
  SetLoading: (data) => false,

  OpenDialogDesconto: false,
  SetOpenDialogDesconto: (data) => false,

  OpenMenuPdv: false,
  SetMenuPdv: (data) => false,

  PdvIsOpen: false,
  SetPdvIsOpen: (data) => true,

  OpenListaOrc: false,
  SetOpenListaOrc: (data) => false,

  OpenFormasDePagamentos: false,
  SetOpenFormasDePagamentos: (data) => false,

  G_StatusVendaAv: false,
  SetStatusVendasAv: (data) => false,

  G_StatusVendaPl: false,
  SetStatusVendasPl: (data) => false,

  G_DialogQuestion: false,
  SetDialogQuestion: (data) => false,

  G_DialogWarning: false,
  SetDialogWarning: (data) => false,

  G_DialogError: false,
  SetDialogError: (data) => false,

  OpenDialogEncerramento: false,
  SetOpenDialogEncerramento: (data: boolean) => false,

  G_ConfigInitial: false,
  SetConfigInitial: (data: boolean) => false,

  G_OpenListaVendaSuspensa: false,
  SetOpenListaVendaSuspensa: (data: boolean) => false,

  G_StatusCaixa: '',
  SetGStatusCaixa: (data: string) => '',

  G_IdTurno: 0,
  SetGIdTurno: (data: number) => 0,

  LoggedIn: false,
  SetLoggedIn: (data) => false,

  G_TotalIteChecked: 0,
  SetGTotalIteChecked: (data: number) => 0,
}

type IAction = { type: 'RESET' }

type Dispatch = (action: IAction) => void

const ClearReducer = (state: TypesContext, action: IAction) => {
  if (action.type === 'RESET') {
    return ValueInitial
  }

  const result: TypesContext = ValueInitial
  return state
}

const AppContext = createContext<TypesContext>(ValueInitial)
const DispatchContext = createContext<Dispatch | undefined>(undefined)

export function AppContextProvider({ children }: PropsWithChildren<{}>) {
  const [state, dispatch] = useReducer(ClearReducer, ValueInitial)
  const [ListVenda, SetListVenda] = useState(state.ListVenda)
  const [Resta, SetResta] = useState(0)
  const [PdvIsOpen, SetPdvIsOpen] = useState(false)
  const [Pagamentos, SetPagamentos] = useState<[]>([])
  const [G_ListOrc, SetListOrc] = useState(state.G_ListOrc)
  const [G_CpfCnpj, SetCpfCnpj] = useState(state.G_CpfCnpj)
  const [G_ValidateCpfCnpj, SetValidateCpf] = useState(state.G_ValidateCpfCnpj)
  const [FormaDePagamento, SetFormaDePagamento] = useState<number>(1)
  const [SelectedItem, SetSelectedItem] = useState<IVendas_Itens>(initialStateItens)
  const [OpenDialogMenuSat, SetOpenDialogMenuSat] = useState<boolean>(false)
  const [OpenDialogSangria, SetOpenDialogSangria] = useState<boolean>(false)
  const [G_ValorSanSup, SetValorSanSup] = useState('');
  const [OpenDialogCPF, SetOpenDialogCPF] = useState<boolean>(false)
  const [OpenDialogDesconto, SetOpenDialogDesconto] = useState<boolean>(false)
  const [Loading, SetLoading] = useState<boolean>(true)
  const [OpenMenuPdv, SetMenuPdv] = useState<boolean>(false)
  const [OpenListaOrc, SetOpenListaOrc] = useState<boolean>(false)
  const [OpenFormasDePagamentos, SetOpenFormasDePagamentos] = useState<boolean>(false)
  const [G_StatusVendaAv, SetStatusVendasAv] = useState(state.G_StatusVendaAv)
  const [G_StatusVendaPl, SetStatusVendasPl] = useState(state.G_StatusVendaPl)
  const [G_DialogQuestion, SetDialogQuestion] = useState(state.G_DialogQuestion)
  const [G_DialogWarning, SetDialogWarning] = useState(state.G_DialogWarning)
  const [G_DialogError, SetDialogError] = useState(state.G_DialogError)
  const [G_Venda, SetGVenda] = useState<IVendas>(initialStateVenda)
  const [G_DadosConfigPdv, SetGDadosPdv] = useState<IConfigPDV>(initialDadosConfig)
  const [G_PosicaoProdTable, SetPosicaoProdTable] = useState(-1)
  const [G_PosicaoProdTableClick, SetPosicaoProdTableClick] = useState(-1)
  const [G_DescMontagemCarrinho, SetDescMontagemCarrinho] = useState(false)
  const [G_DescFinalizarVenda, SetDescFinalizarVenda] = useState(false)
  const [G_ValorDesc, SetValorDesc] = useState('');
  const [G_PercentualDesc, SetPercentualDesc] = useState('');
  const [G_CheckedItem, SetCheckedItem] = useState<number[]>([])
  const [OpenDialogEncerramento, SetOpenDialogEncerramento] = useState(false)
  const [G_ConfigInitial, SetConfigInitial] = useState(false)
  const [G_OpenListaVendaSuspensa, SetOpenListaVendaSuspensa] = useState(false)
  const [G_StatusCaixa, SetGStatusCaixa] = useState('')
  const [G_IdTurno, SetGIdTurno] = useState(0)
  const [LoggedIn, SetLoggedIn] = useState(false)
  const [G_TotalIteChecked, SetGTotalIteChecked] = useState(0)

  const TipoSangriaSup = ''

  const value = {
    ListVenda,
    SetListVenda,
    G_ListOrc,
    SetListOrc,
    G_CpfCnpj,
    SetCpfCnpj,
    G_ValidateCpfCnpj,
    SetValidateCpf,
    FormaDePagamento,
    SetFormaDePagamento,
    SelectedItem,
    SetSelectedItem,
    OpenDialogMenuSat,
    SetOpenDialogMenuSat,
    OpenDialogSangria,
    G_ValorSanSup,
    SetValorSanSup,
    SetOpenDialogSangria,
    TipoSangriaSup,
    OpenMenuPdv,
    SetMenuPdv,
    OpenListaOrc,
    SetOpenListaOrc,
    G_StatusVendaAv,
    SetStatusVendasAv,
    G_StatusVendaPl,
    SetStatusVendasPl,
    OpenDialogCPF,
    SetOpenDialogCPF,
    OpenDialogDesconto,
    SetOpenDialogDesconto,
    OpenFormasDePagamentos,
    SetOpenFormasDePagamentos,
    G_DialogQuestion,
    SetDialogQuestion,
    G_DialogWarning,
    SetDialogWarning,
    G_DialogError,
    SetDialogError,
    Pagamentos,
    SetPagamentos,
    Resta,
    SetResta,
    Loading,
    SetLoading,
    PdvIsOpen,
    SetPdvIsOpen,
    G_Venda,
    SetGVenda,
    G_PosicaoProdTable,
    SetPosicaoProdTable,
    G_PosicaoProdTableClick,
    SetPosicaoProdTableClick,
    G_DescMontagemCarrinho,
    SetDescMontagemCarrinho,
    G_DescFinalizarVenda,
    SetDescFinalizarVenda,
    G_ValorDesc,
    SetValorDesc,
    G_PercentualDesc,
    SetPercentualDesc,
    G_CheckedItem,
    SetCheckedItem,
    OpenDialogEncerramento,
    SetOpenDialogEncerramento,
    G_ConfigInitial,
    SetConfigInitial,
    G_DadosConfigPdv,
    SetGDadosPdv,
    G_OpenListaVendaSuspensa,
    SetOpenListaVendaSuspensa,
    G_StatusCaixa, SetGStatusCaixa,
    G_IdTurno, SetGIdTurno,
    LoggedIn, SetLoggedIn,
    G_TotalIteChecked, SetGTotalIteChecked
  }

  return (
    <AppContext.Provider value={value}>
      <DispatchContext.Provider value={dispatch}>{children}</DispatchContext.Provider>
    </AppContext.Provider>
  )
}

export const useDispatch = (): Dispatch => {
  const context = useContext(DispatchContext)

  if (context === undefined) {
    throw new Error('useAuthDispatch deve ser usado dentro de um AuthProvider')
  }

  return context
}

export const useAppContext = (): TypesContext => {
  const context = useContext(AppContext)

  if (context === undefined) {
    throw new Error('useAuthState deve ser usado dentro de um AuthProvider')
  }

  return context
}

export const useClear = (): [TypesContext, Dispatch] => {
  return [useAppContext(), useDispatch()]
}

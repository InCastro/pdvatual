import { useEffect } from "react";

export default function UseKeyPress(
  key: string,
  action: (e: string) => void
): void {
  useEffect(() => {
    function onKeyup(e: any): void {
      if (e.key === key) action(e.key);
    }
    window.addEventListener("keyup", onKeyup);
    return () => window.removeEventListener("keyup", onKeyup);
  });
}

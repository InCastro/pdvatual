import React, { useCallback, useEffect, useState } from 'react'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import { TextField, Divider } from '@material-ui/core'
import Button from '@material-ui/core/Button'

import { CustomTable } from '../Table'
import styles from './Principal.module.css'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { useAppContext } from '../../context/AppContexts'
import UseKeyPress from '../../hooks/useKeyPress'
import axios from 'axios'
import { UrlAPI } from './../../utils'

import { formatters, round } from '../../utils'
import Snackbar from '@material-ui/core/Snackbar'
import { Alert, AlertTitle } from '@material-ui/lab'
import { useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'

export function TelaPrincipal() {
  const [OpenAlert, SetOpenAlert] = useState(false)
  const [ErrorQtd, SetErrorQtd] = useState(false)
  const [ErrorPrd, SetErrorPrd] = useState(false)
  const [Ean, SetEan] = useState('')
  const [Qtd, SetQtd] = useState('')
  const {
    G_StatusVendaAv,
    G_StatusVendaPl,
    SetLoading,
    SetResta,
    G_Venda,
    SetPosicaoProdTable,
    G_PosicaoProdTableClick,
    SetPosicaoProdTableClick,
    SetSelectedItem,
    SetDescMontagemCarrinho,
    SetDescFinalizarVenda,
    SetConfigInitial,
    G_DadosConfigPdv,
    G_IdTurno,
    SetGIdTurno,
  } = useAppContext()

  const history = useHistory()

  const initKeyPress = () => {
    UseKeyPress('F10', (key: string) => {
      ; (document.getElementById('buttonPagar') as HTMLButtonElement).click()
    })

    UseKeyPress('Enter', (key: string) => {
      SetErrorQtd(Qtd.trim() === '')
      SetErrorPrd(Ean.trim() === '')
      if (Ean.substring(0, Ean.indexOf('*')) !== '') {
        SetQtd(Ean.substring(0, Ean.indexOf('*')))
        SetErrorQtd(false)
        SetEan(Ean.substring(Ean.indexOf('*') + 1))
      }

      var inputFocus = document.activeElement
      if (G_StatusVendaAv || G_StatusVendaPl) {
        if (inputFocus?.id === 'inputProduto') {
          ; (document.getElementById('inputQtd') as HTMLElement).focus()
        }
        if (inputFocus?.id === 'inputQtd') {
          getItemPl(Ean, parseInt(Qtd.trim() === '' ? '0' : Qtd))
            ; (document.getElementById('inputProduto') as HTMLElement).focus()
        }
      }
    })
  }

  initKeyPress()

  useEffect(() => {
    SetLoading(false)
    let IdTurno: number;
    const method = 'get'
    const url = `${UrlAPI}carregarParametros`
    if (G_DadosConfigPdv.COD_ATVSAT === '') {
      axios[method](url).then((res) => {
        if (res.data === '') {
          SetConfigInitial(true);
          history.push('./configPdv')
        } else {
          SetConfigInitial(false);
          G_DadosConfigPdv.VALOR_SANGRIA = res.data.Valor_Sangria;
          G_DadosConfigPdv.CNPJ_EMPRESA = res.data.CNPJ;
          G_DadosConfigPdv.IE = res.data.IE;
          G_DadosConfigPdv.ASS_SAT = res.data.Cod_AssSat;
          G_DadosConfigPdv.COD_ATVSAT = res.data.Codigo_AtvSat;
          G_DadosConfigPdv.NUM_CAIXA = res.data.Num_Caixa;
          G_DadosConfigPdv.CEP_EMPRESA = res.data.CEP;
          G_DadosConfigPdv.RAZAO_SOCIAL = res.data.Descricao_Empresa;
          G_DadosConfigPdv.FANTASIA = res.data.Fantasia;
          G_DadosConfigPdv.ENDERECO_EMPRESA = res.data.Endereco_Empresa;
          G_DadosConfigPdv.FONE_EMPRESA = res.data.Fone;
          setTimeout(() => {
            SetLoading(false)
            SetDescMontagemCarrinho(true)
            SetDescFinalizarVenda(false)
            const url3 = `${UrlAPI}idTurno`
            axios[method](url3).then((res) => {
              if (res.data.length === 0) {
                history.push('./login')
              } else {
                SetGIdTurno(res.data[0].Id_Turno)
                IdTurno = res.data[0].Id_Turno;
                const url2 = `${UrlAPI}alertasangria/${IdTurno}`
                axios[method](url2).then((res) => {
                  if (!G_StatusVendaAv && !G_StatusVendaPl) {
                    if (res.data[0].Valor > G_DadosConfigPdv.VALOR_SANGRIA) {
                      Swal.fire({
                        icon: 'info',
                        text: 'Favor, realizar sangria!',
                      })
                    }
                  }
                })
              }
            })

          }, 1000)
        }
      })
    }
  }, [])

  const getItem = () => {
    return (
      G_Venda.ITENS.filter(function (prod, indexprod) {
        return indexprod === G_PosicaoProdTableClick
      })[0] || []
    )
  }

  const OpeAlert = () => {
    SetOpenAlert(true)
  }

  const CloseAlert = () => {
    SetOpenAlert(false)
  }

  const getItemPl = useCallback(async (Ean: string, Quantidade: number) => {
    if (Ean !== '' && Quantidade !== 0) {
      const method = 'get'
      const url = `${UrlAPI}consultaProduto/${Ean}`
      let index: number
      SetLoading(true)
      await axios[method](url)
        .then((res) => {
          if (res.data.Response === 'Produto não encontrado') {
            OpeAlert()
            SetEan('')
          } else {
            index = G_Venda.ITENS.length
            SetPosicaoProdTable(G_Venda.ITENS.length)
            SetPosicaoProdTableClick(G_Venda.ITENS.length)
            G_Venda.ITENS.push({
              DESCRICAO: res.data.Item.DESCRICAO,
              KEY_RESERVAPROD: '',
              QUANTIDADE: Quantidade,
              SKU: res.data.Item.SKU,
              EAN: res.data.Item.EAN,
              VALOR_FRETE: 0,
              VALOR_UNITARIO: res.data.Item.VALOR_UNITARIO,
              DESCONTO: 0,
              DESCONTO_RATEIO: 0,
              VALOR_ITEM: res.data.Item.VALOR_UNITARIO * Quantidade,
              ITEM_AV: false,
              INDEX: index,
              CHECKED: false,
            })
            G_Venda.ID_TURNO = G_IdTurno;
            G_Venda.QUANTIDADE_TOTAL = G_Venda.QUANTIDADE_TOTAL + Quantidade
            G_Venda.VALOR_ITENS_PL = G_Venda.VALOR_ITENS_PL + Quantidade * res.data.Item.VALOR_UNITARIO
            G_Venda.VALOR_VENDA_PL = G_Venda.VALOR_ITENS_PL - G_Venda.VALOR_DESCONTO_PL
            const data = G_Venda.ITENS.filter(function (prod, indexprod) {
              return indexprod === index
            })
            SetSelectedItem(data)
            SetEan('')
            SetQtd('')
            SetLoading(false)
          }
        })
        .catch((res) => {
          SetLoading(false)
        })
    }
  }, [])

  return (
    <>
      <div style={{ flexGrow: 1 }}>
        <Grid>
          <Grid item xs={12} className={styles.Grid}>
            <Card style={{ backgroundColor: '#FAFAFA' }} className={styles.Card1} variant="outlined" square>
              <br />
              <Grid container spacing={3} style={{ height: '100%', maxHeight: '100%' }}>
                <Grid item xs={7} style={{ height: '100%', maxHeight: '100%' }}>
                  <CustomTable />
                </Grid>
                <Grid item xs={5}>
                  <Card style={{ marginRight: 10, height: 'calc(100% - 125px)' }}>
                    <Grid container spacing={1}>
                      <Grid item xs={12}>
                        <Card
                          style={{
                            height: 150,
                            backgroundColor: '#6022a9',
                            padding: 20,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                          variant="outlined"
                        >
                          <Grid container>
                            {/* {getItem().SKU && ( */}
                            <Grid item xs={12}>
                              <h4
                                style={{
                                  color: '#ffffff',
                                  fontSize: 10,
                                  marginBottom: 5,
                                }}
                              >
                                {getItem().DESCRICAO ? 'DESCRIÇÃO DO PRODUTO' : 'BOAS COMPRAS'}
                              </h4>
                            </Grid>
                            {/* )} */}
                            <Grid item xs={12}>
                              <h4
                                style={{
                                  color: '#ffffff',
                                  fontSize: 28,
                                  fontWeight: 500,
                                }}
                              >
                                {getItem().DESCRICAO || 'Adicione/Selecione um produto ou serviço'}
                              </h4>
                            </Grid>
                            {getItem().SKU && (
                              <Grid item xs={12}>
                                <h4
                                  style={{
                                    color: '#ffffff',
                                    fontSize: 16,
                                    marginTop: 15,
                                  }}
                                >
                                  SKU: {getItem().SKU}
                                </h4>
                              </Grid>
                            )}
                          </Grid>
                        </Card>
                      </Grid>

                      <Grid item xs={9} style={{ padding: '20px 0 20px 20px' }}>
                        <TextField
                          style={{ width: '100%' }}
                          id="inputProduto"
                          variant="outlined"
                          placeholder="ESCANEAR PRODUTO"
                          error={ErrorPrd}
                          helperText={ErrorPrd ? 'Informar EAN ou SKU' : ''}
                          value={Ean}
                          disabled={!G_StatusVendaAv && !G_StatusVendaPl}
                          onChange={(e) => {
                            SetEan(e.target.value)
                            SetErrorPrd(false)
                          }}
                        />
                      </Grid>

                      <Grid item xs={3} style={{ padding: 20 }}>
                        <TextField
                          style={{ width: '100%' }}
                          id="inputQtd"
                          variant="outlined"
                          placeholder="QTDE."
                          error={ErrorQtd}
                          helperText={ErrorQtd ? 'Informar Qtde.' : ''}
                          value={Qtd}
                          size="medium"
                          disabled={!G_StatusVendaAv && !G_StatusVendaPl}
                          onChange={(e) => {
                            SetQtd(e.target.value)
                            SetErrorQtd(false)
                          }}
                        />
                      </Grid>

                      <Grid item xs={12}>
                        <AppBar position="static" style={{ background: '#ffffff' }}>
                          <Toolbar variant="dense">
                            <Typography variant="h6" style={{ color: '#000000' }}>
                              Resumo do Item
                            </Typography>
                          </Toolbar>
                        </AppBar>
                        <Grid container spacing={1} style={{ padding: 20 }}>
                          <Grid style={{ marginTop: 10 }} item xs={6}>
                            <label style={{ fontSize: 16 }}>VALOR UNITÁRIO</label>
                          </Grid>

                          <Grid style={{ marginTop: 10, textAlign: 'right' }} item xs={6}>
                            <label style={{ fontSize: 16 }}>
                              {formatters.formatMoney(getItem().VALOR_UNITARIO || 0)}
                            </label>
                          </Grid>

                          <Grid style={{ marginTop: 10 }} item xs={6}>
                            <label style={{ fontSize: 16 }}>DESCONTO</label>
                          </Grid>

                          <Grid style={{ marginTop: 10, textAlign: 'right' }} item xs={6}>
                            <label style={{ fontSize: 16 }}>{formatters.formatMoney(getItem().DESCONTO)}</label>
                          </Grid>

                          <Grid style={{ marginTop: 20 }} item xs={6}>
                            <b>
                              <label style={{ fontSize: 20, fontWeight: 'bold' }}>TOTAL ITEM</label>
                            </b>
                          </Grid>

                          <Grid style={{ marginTop: 20, textAlign: 'right' }} item xs={6}>
                            <label style={{ fontSize: 20, fontWeight: 'bold' }}>
                              {formatters.formatMoney(getItem().VALOR_ITEM)}
                            </label>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Card>
                  <Divider style={{ marginRight: 10 }} />
                  <Grid>
                    <Card style={{ marginRight: 10, height: '95px', padding: 20 }}>
                      <Grid container spacing={1} style={{ height: '100%', margin: 0 }}>
                        <Grid item xs={6} style={{ height: '100%' }}>
                          <Button
                            id={'buttonPagar'}
                            className="actions"
                            disabled={G_Venda.QUANTIDADE_TOTAL === 0}
                            onClick={() => {
                              SetResta(round(G_Venda.VALOR_VENDA_PL + G_Venda.VALOR_VENDA_PL, 2));
                              G_Venda.VALOR_TOTAL = round(G_Venda.VALOR_VENDA_AV + G_Venda.VALOR_VENDA_PL, 2);
                              history.push('/finalizar')
                            }}
                            style={{
                              backgroundColor: '#3bd553',
                              color: '#FFF',
                              width: '100%',
                              height: '100%',
                            }}
                          >
                            FINALIZAR VENDA (F10)
                          </Button>
                        </Grid>

                        <Grid item xs={6} style={{ height: '100%' }}>
                          <Button
                            className="actions"
                            disabled={G_Venda.QUANTIDADE_TOTAL === 0}
                            style={{
                              backgroundColor: '#6022a9',
                              color: '#FFF',
                              width: '100%',
                              height: '100%',
                            }}
                          >
                            INSERIR CUPOM DESCONTO (F12)
                          </Button>
                        </Grid>
                      </Grid>
                    </Card>
                  </Grid>
                </Grid>
              </Grid>
            </Card>
          </Grid>
          <Snackbar open={OpenAlert} autoHideDuration={4000} onClose={CloseAlert}>
            <Alert style={{ width: 500, height: 100 }} onClose={CloseAlert} severity="error">
              <AlertTitle>Error</AlertTitle>
              Produto não encontrado.
            </Alert>
          </Snackbar>
        </Grid>
      </div>
    </>
  )
}

export default TelaPrincipal

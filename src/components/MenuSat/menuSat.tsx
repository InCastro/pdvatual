import React, { useState, useCallback } from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import axios from 'axios'
import { Alert, AlertTitle } from '@material-ui/lab'
import Snackbar from '@material-ui/core/Snackbar'
import { useAppContext } from './../../context/AppContexts'
import { UrlAPI } from './../../utils'
import Swal from 'sweetalert2'

export function MenuSat() {
    const [OpenAlert, SetOpenAlert] = useState(false)
    let responseSat: string;
    const {
        OpenDialogMenuSat,
        SetOpenDialogMenuSat,
    } = useAppContext()

    const handleClose = () => {
        SetOpenDialogMenuSat(false)
    }

    const CloseAlert = () => {
        SetOpenAlert(false)
    }

    const getStatusSat = useCallback(async () => {
        handleClose()
        const method = 'get'
        const url = `${UrlAPI}consultarSat`
        await axios[method](url).then((res) => {
            console.log(res.data)
            if (res.data.message === 'error') {
                Swal.fire({
                    icon: 'error',
                    text: res.data.response,
                })
            } else {
                Swal.fire({
                    icon: 'success',
                    text: res.data.response,
                })
            }
        })
    }, [])

    return (
        <>
            <Dialog open={OpenDialogMenuSat} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle>
                    MENU SAT
                </DialogTitle>
                <DialogContent style={{ height: 500, width: 500 }} dividers>
                    <Button style={{ backgroundColor: '#6022a9', color: '#FFF', width: '100%' }} id={'buttonCancelar'} onClick={getStatusSat} color="primary">
                        1 - Consultar S@t
                    </Button>
                    <br />
                    <br />
                    <Button style={{ backgroundColor: '#6022a9', color: '#FFF', width: '100%' }} id={'buttonCancelar'} onClick={handleClose} color="primary">
                        2 - Reimpressão Extrato S@T
                    </Button>
                </DialogContent>
                <DialogActions>
                    <Button id={'buttonCancelar'} onClick={handleClose} color="primary">
                        Cancelar (ESC)
                    </Button>
                </DialogActions>
            </Dialog>
            <Snackbar open={OpenAlert} autoHideDuration={6000} onClose={CloseAlert}>
                <Alert onClose={CloseAlert} severity="info">
                    <AlertTitle>info</AlertTitle>

                </Alert>
            </Snackbar>
        </>
    )
}

export default MenuSat;
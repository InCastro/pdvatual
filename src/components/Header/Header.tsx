import React, { useState } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { useLocation } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import BallotIcon from '@material-ui/icons/Ballot'
import MenuIcon from '@material-ui/icons/Menu'
import RecentActorsIcon from '@material-ui/icons/RecentActors'
import ListAltIcon from '@material-ui/icons/ListAlt'
import MoneyOffIcon from '@material-ui/icons/MoneyOff'
import Button from '@material-ui/core/Button'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Menu, { MenuProps } from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import PaymentIcon from '@material-ui/icons/Payment'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import DeleteIcon from '@material-ui/icons/Delete'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import DoneOutlineIcon from '@material-ui/icons/DoneOutline'
import SearchIcon from '@material-ui/icons/Search';
import BuildIcon from '@material-ui/icons/Build';
import { useHistory } from 'react-router-dom'
import CachedIcon from '@material-ui/icons/Cached';

import { useAppContext } from '../../context/AppContexts'
import { SangriaSuprimento } from '../SagriaSuprimento'
import { Orcamento } from './../Orcamentos'
import { FormasDePagamentos } from '../FormasDePagamentos'
import { CpfCnpj } from '../CpfCnpj'
import { DescontoGeral } from '../Desconto'
import { DialogQuestion } from '../DialogQuestion'
import { MenuSat } from '../MenuSat'
import { Encerramento } from '../Encerramento'
import { VendaSuspensa } from '../CarregarVendaSuspensa'
import Swal from 'sweetalert2'
import { UrlAPI } from '../../utils'
import axios from 'axios'

import UseKeyPress from '../../hooks/useKeyPress'
import LogoImg from '../../assets/mobly_logo.png'

import './Header.module.css'

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props: MenuProps) => (
  <Menu
    elevation={2}
    style={{ marginTop: 10 }}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    MenuListProps={{ disablePadding: true }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
))

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '& span': {
      fontSize: 12,
    },
    '&:focus': {
      backgroundColor: '#6022a9',
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem)

export const Header = () => {
  const history = useHistory()
  const location = useLocation()
  const blockLocation = location.pathname === '/finalizar' || location.pathname === '/configPdv' || location.pathname === '/login'
  const blockLocationConfig = location.pathname === '/configPdv' || location.pathname === '/login'
  const [TipoSanSup, SetTipoSanSup] = useState<string>('')
  const [DialogProducts, SetOpenDialogProducts] = useState(false)
  const {
    SetOpenDialogSangria,
    G_StatusVendaAv,
    G_StatusVendaPl,
    SetOpenDialogCPF,
    G_CpfCnpj,
    SetValorDesc,
    SetPercentualDesc,
    SetStatusVendasAv,
    SetStatusVendasPl,
    SetValorSanSup,
    SetOpenDialogDesconto,
    SetDialogQuestion,
    SetOpenDialogMenuSat,
    SetOpenDialogEncerramento,
    SetCpfCnpj,
    SetPagamentos,
    SetResta,
    SetListOrc,
    G_Venda,
    SetLoading,
    SetConfigInitial,
    ListVenda,
    SetListVenda,
    SetOpenListaVendaSuspensa,
    G_IdTurno
  } = useAppContext()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const SangriaOpen = () => {
    setAnchorEl(null)
    SetOpenDialogSangria(true)
    SetTipoSanSup('SAN')
    SetValorSanSup('');
  }

  const SuprimentoOpen = () => {
    setAnchorEl(null)
    SetOpenDialogSangria(true)
    SetTipoSanSup('SUP')
    SetValorSanSup('');
  }

  const MenuSatOpen = () => {
    setAnchorEl(null)
    SetOpenDialogMenuSat(true)
  }

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })

  const initKeyPress = () => {
    UseKeyPress('F1', (key: string) => {
      alert(`you pressed ${key}!`)
    })

    UseKeyPress('F2', (key: string) => {
      ; (document.getElementById('buttonMenu') as HTMLButtonElement).click()
    })

    UseKeyPress('F3', (key: string) => {
      ; (document.getElementById('buttonPL') as HTMLButtonElement).click()
    })

    UseKeyPress('F4', (key: string) => {
      ; (document.getElementById('buttonCpf') as HTMLButtonElement).click()
    })

    UseKeyPress('F7', (key: string) => {
      ; (document.getElementById('buttonAv') as HTMLButtonElement).click()
    })

    UseKeyPress('F8', (key: string) => {
      ; (document.getElementById('buttonDesconto') as HTMLButtonElement).click()
    })
  }

  const OpenMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const CloseMenu = () => {
    setAnchorEl(null)
  }

  const OpenModalCancelProducts = () => {
    SetOpenDialogProducts(true)
  }
  const handleClose = () => {
    SetOpenDialogProducts(false)
  }

  const OpenConfigPdv = () => {
    SetConfigInitial(false);
    setAnchorEl(null)
    history.push('./configPdv')
  }

  const CarregarVendaSuspensa = () => {
    let venda = Array.from(ListVenda)
    SetLoading(true)
    const method = 'get';
    const url = `${UrlAPI}suspenderVendaCab`;
    axios[method](url)
      .then((response) => {
        if (response.data.Venda.length !== 0) {
          venda = response.data.Venda;
          console.log(venda)
          venda.map((item => {
            const url2 = `${UrlAPI}suspenderVendaIte/${item.ID_VENDA}`
            axios[method](url2).then((res) => {
              ListVenda.push({
                COD_AV: item.COD_AV,
                ID_TURNO: G_IdTurno,
                COD_OPERADOR: item.COD_OPERADOR,
                COD_VENDEDOR: item.COD_VENDEDOR,
                QUANTIDADE_TOTAL: item.QUANTIDADE_TOTAL,
                VALOR_DESCONTO_AV: item.VALOR_DESCONTO_AV,
                VALOR_DESCONTO_PL: item.VALOR_DESCONTO_PL,
                VALOR_FRETE: item.VALOR_FRETE,
                VALOR_ITENS_AV: 0,
                VALOR_ITENS_PL: 0,
                VALOR_TOTAL: item.VALOR_TOTAL,
                VALOR_VENDA_AV: item.VALOR_VENDA_AV,
                VALOR_VENDA_PL: item.VALOR_VENDA_PL,
                ID_VENDA: item.ID_VENDA,
                ITENS: res.data
              })
            })
          }))
        }
      }).catch((erro) => {
        SetLoading(false);
      })
    setTimeout(() => {
      SetLoading(false)
      setAnchorEl(null)
      SetOpenListaVendaSuspensa(true);
    }, 1000)

  }

  const confirmProducts = () => { }

  initKeyPress()

  const dialogCPF = () => {
    SetOpenDialogCPF(true)
  }

  const PegueLeve = () => {
    SetStatusVendasPl(true)
    SetStatusVendasAv(false)
  }

  const dialogDesconto = () => {

    SetOpenDialogDesconto(true)
    SetValorDesc('')
    SetPercentualDesc('')
  }

  const SuspenderVenda = () => {
    //SetDialogQuestion(true)
    swalWithBootstrapButtons.fire({
      title: 'Suspender Venda?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        SetLoading(true)
        const method = 'post';
        const url = `${UrlAPI}suspenderVenda`;
        axios[method](url, G_Venda)
          .then((response) => {
            SetStatusVendasAv(false)
            SetStatusVendasPl(false)
            SetCpfCnpj('')
            SetPagamentos([])
            SetResta(0)
            SetListOrc([])
            SetDialogQuestion(false)
            G_Venda.ID_VENDA = 0;
            G_Venda.ITENS = [];
            G_Venda.PAGAMENTO = [];
            G_Venda.QUANTIDADE_TOTAL = 0;
            G_Venda.VALOR_DESCONTO_AV = 0;
            G_Venda.VALOR_DESCONTO_PL = 0;
            G_Venda.VALOR_FRETE = 0;
            G_Venda.VALOR_ITENS_AV = 0;
            G_Venda.VALOR_ITENS_PL = 0;
            G_Venda.VALOR_VENDA_AV = 0;
            G_Venda.VALOR_VENDA_PL = 0;
            G_Venda.VALOR_TOTAL = 0;
            setTimeout(() => {
              SetLoading(false)
              swalWithBootstrapButtons.fire(
                'Salvo!',
                'Venda suspensa com sucesso.',
                'success'
              )
            }, 1000)
          })
          .catch((error) => {
            setTimeout(() => {
              SetLoading(false)
              swalWithBootstrapButtons.fire(
                'Erro!',
                `Erro ao suspender venda. ${error}`,
                'error'
              )
            }, 1000)
          })

      }
    })
    setAnchorEl(null)
  }

  const dialogEncerramento = () => {
    SetOpenDialogEncerramento(true)
    setAnchorEl(null)
  }

  return (
    <Grid item xs={12}>
      <AppBar
        style={{
          height: 100,
          backgroundColor: '#6022a9',
          borderBottom: '10px solid #ff4600',
        }}
        variant="elevation"
        position="static"
      >
        <Toolbar>
          <Grid container spacing={1}>
            <Grid item xs={1} style={{ textAlign: 'center' }}>
              <img style={{ marginTop: 30 }} src={LogoImg} alt="Mobly" />
            </Grid>

            <Grid item xs={11}>
              <div style={{ marginTop: 28, marginLeft: 10 }}>
                <Button
                  id={'buttonMenu'}
                  disabled={blockLocationConfig}
                  onClick={OpenMenu}
                  className="header_item"
                  endIcon={<MenuIcon />}
                  size="medium"
                >
                  Menu (F2)
                </Button>
                <Button
                  id={'buttonPL'}
                  disabled={G_StatusVendaAv || G_StatusVendaPl || blockLocation}
                  className="header_item"
                  endIcon={<ListAltIcon />}
                  size="medium"
                  onClick={PegueLeve}
                >
                  Pegue e Leve (F3)
                </Button>
                <Button
                  id={'buttonCpf'}
                  disabled={G_CpfCnpj !== '' || !G_StatusVendaPl || blockLocation}
                  className="header_item"
                  endIcon={<RecentActorsIcon />}
                  size="medium"
                  onClick={dialogCPF}
                >
                  Ident. Consumidor (F4)
                </Button>
                <Button
                  disabled={G_StatusVendaAv || blockLocation}
                  id={'buttonAv'}
                  onClick={dialogCPF}
                  className="header_item"
                  endIcon={<BallotIcon />}
                  size="medium"
                >
                  AV (F7)
                </Button>
                <Button
                  id={'buttonDesconto'}
                  disabled={(!G_StatusVendaAv && !G_StatusVendaPl || blockLocation)}
                  className="header_item"
                  endIcon={<MoneyOffIcon />}
                  size="medium"
                  onClick={dialogDesconto}
                >
                  Desconto (F8)
                </Button>
                <Button
                  id={'buttonBuscarPreco'}
                  disabled={(G_StatusVendaAv && G_StatusVendaPl || blockLocation)}
                  className="header_item"
                  endIcon={<SearchIcon />}
                  size="medium"
                >
                  Buscar Preço (F9)
                </Button>
              </div>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>

      <div>
        <StyledMenu id="customized-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={CloseMenu}>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl} onClick={SangriaOpen}>
            <ListItemIcon>
              <MoneyOffIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary={'SANGRIA'} />
          </StyledMenuItem>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl} onClick={SuprimentoOpen}>
            <ListItemIcon>
              <AttachMoneyIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="SUPRIMENTO DE CAIXA" />
          </StyledMenuItem>
          <StyledMenuItem disabled={!G_StatusVendaAv && !G_StatusVendaPl}>
            <ListItemIcon>
              <DeleteIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="CANCELAR ITEM" />
          </StyledMenuItem>
          <StyledMenuItem disabled={!G_StatusVendaAv && !G_StatusVendaPl} onClick={SuspenderVenda}>
            <ListItemIcon>
              <DeleteForeverIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="SUSPENDER VENDA" />
          </StyledMenuItem>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl} onClick={CarregarVendaSuspensa}>
            <ListItemIcon>
              <CachedIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="CARREGAR VENDA SUSPENSA" />
          </StyledMenuItem>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl}>
            <ListItemIcon>
              <DeleteForeverIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText onClick={OpenModalCancelProducts} primary="CANCELAR VENDA" />
          </StyledMenuItem>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl} onClick={MenuSatOpen}>
            <ListItemIcon>
              <MenuIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="MENU SAT" />
          </StyledMenuItem>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl}>
            <ListItemIcon>
              <PaymentIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="MENU TEF" />
          </StyledMenuItem>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl} onClick={dialogEncerramento}>
            <ListItemIcon>
              <DoneOutlineIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="ENCERRAMENTO DE CAIXA" />
          </StyledMenuItem>
          <StyledMenuItem disabled={G_StatusVendaAv || G_StatusVendaPl} onClick={OpenConfigPdv}>
            <ListItemIcon>
              <BuildIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="CONFIG. DE CAIXA" />
          </StyledMenuItem>
        </StyledMenu>
      </div>
      <SangriaSuprimento Tipo={TipoSanSup} />
      <Orcamento />
      <CpfCnpj />
      <FormasDePagamentos />
      <DescontoGeral />
      <DialogQuestion Question={'Deseja suspender a venda?'} TipoAcao={'Suspender'} />
      <MenuSat />
      <Encerramento />
      <VendaSuspensa />
    </Grid>
  )
}

export default Header

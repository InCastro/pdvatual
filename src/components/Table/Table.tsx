import React, { useState, useCallback } from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { Paper, Typography, Tooltip, Grid, IconButton } from '@material-ui/core'
import { useAppContext } from '../../context/AppContexts'
import Card from '@material-ui/core/Card'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import { formatters, round } from '../../utils'
import { Style } from '@material-ui/icons'
import EmptyCart from '../../assets/icon-empty-cart.svg'
import Checkbox from '@material-ui/core/Checkbox'
import DeleteIcon from '@material-ui/icons/Delete'
import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core/styles'
import Swal from 'sweetalert2'

export const CustomTable = () => {
  const {
    ListVenda,
    SelectedItem,
    SetSelectedItem,
    G_Venda,
    G_PosicaoProdTable,
    G_PosicaoProdTableClick,
    SetPosicaoProdTableClick,
    SetPosicaoProdTable,
    SetLoading,
    SetGTotalIteChecked,
    G_TotalIteChecked
  } = useAppContext()
  const [DialogProduts, SetOpenDialogProduts] = useState(false)
  const [rowsProduts, setrowsProduts] = useState([{}])
  const [totChecked, SettotChecked] = useState(0)

  const useToolbarStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
      },
      highlight:
        theme.palette.type === 'light'
          ? {
            color: '#6022a9',
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
          : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
      title: {
        flex: '1 1 100%',
      },
    })
  )

  const classes = useToolbarStyles()

  const handleClose = () => {
    SetOpenDialogProduts(false)
  }

  const handleTrue = () => {
    console.log(SelectedItem)
    rowsProduts.includes(SelectedItem) == false ? rowsProduts.push(SelectedItem) : console.log('já tem')
    console.log(rowsProduts)

    // SetOpenDialogProduts(true);
  }

  const handleChecked = (select: boolean, index: number) => {
    G_Venda.ITENS[index].CHECKED = select
    const data = G_Venda.ITENS.filter(function (prod) {
      return prod.CHECKED === true
    })
    SettotChecked(data.length);

    SetGTotalIteChecked(G_TotalIteChecked + (G_Venda.ITENS[index].VALOR_UNITARIO * G_Venda.ITENS[index].QUANTIDADE));
  }

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })

  const handleCancelItem = () => {
    swalWithBootstrapButtons.fire({
      title: 'Cancelar item(s) selecionado(s)?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        const data = G_Venda.ITENS.filter(function (prod) {
          return prod.CHECKED === false || prod.ITEM_AV === true
        })
        G_Venda.ITENS = []
        G_Venda.VALOR_DESCONTO_AV = 0
        G_Venda.VALOR_DESCONTO_PL = 0
        G_Venda.QUANTIDADE_TOTAL = 0
        G_Venda.VALOR_ITENS_AV = 0
        G_Venda.VALOR_ITENS_PL = 0
        G_Venda.VALOR_FRETE = 0
        G_Venda.VALOR_VENDA_AV = 0
        G_Venda.VALOR_VENDA_PL = 0

        data.map((item, index) => {
          G_Venda.ITENS.push({
            DESCRICAO: item.DESCRICAO,
            QUANTIDADE: item.QUANTIDADE,
            SKU: item.SKU,
            EAN: item.EAN,
            VALOR_FRETE: item.VALOR_FRETE,
            VALOR_UNITARIO: item.VALOR_UNITARIO,
            DESCONTO: 0,
            DESCONTO_RATEIO: 0,
            VALOR_ITEM: item.VALOR_UNITARIO * item.QUANTIDADE,
            ITEM_AV: item.ITEM_AV,
            INDEX: G_Venda.ITENS.length,
            CHECKED: false,
            KEY_RESERVAPROD: ''
          })
          G_Venda.QUANTIDADE_TOTAL = G_Venda.QUANTIDADE_TOTAL + item.QUANTIDADE
          G_Venda.ITENS[index].ITEM_AV ? G_Venda.VALOR_ITENS_AV = round(G_Venda.VALOR_ITENS_AV + item.QUANTIDADE * item.VALOR_UNITARIO, 2)
            : G_Venda.VALOR_ITENS_PL = round(G_Venda.VALOR_ITENS_PL + item.QUANTIDADE * item.VALOR_UNITARIO, 2)
          G_Venda.VALOR_FRETE = G_Venda.VALOR_FRETE + item.VALOR_FRETE
          G_Venda.ITENS[index].ITEM_AV ?
            G_Venda.VALOR_VENDA_AV = round(
              G_Venda.VALOR_FRETE + G_Venda.VALOR_ITENS_AV - G_Venda.VALOR_DESCONTO_AV,
              2
            ) :
            G_Venda.VALOR_VENDA_PL = round(
              G_Venda.VALOR_ITENS_PL - G_Venda.VALOR_DESCONTO_PL,
              2
            )
          SetPosicaoProdTable(G_Venda.ITENS.length)
          SetPosicaoProdTableClick(G_Venda.ITENS.length)
        })
        SettotChecked(0)

      }
    })
  }

  return (
    <>
      <TableContainer
        style={{
          marginLeft: 10,
          height: 'calc(100% - 300px)',
          maxHeight: 'calc(100% - 125px)',
        }}
        component={Paper}
      >
        {totChecked > 0 && (
          <Toolbar className={classes.highlight} style={{ backgroundColor: '#fafafa' }} variant="dense">
            {totChecked > 0 ? (
              <Typography className={classes.title} variant="subtitle1" color="inherit">
                {`${totChecked} item(s) selecionado(s)`}
              </Typography>
            ) : (
              ''
            )}

            {totChecked > 0 ? (
              <Tooltip title="Cancelar Item?">
                <IconButton aria-label="delete" onClick={handleCancelItem}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            ) : (
              ''
            )}
          </Toolbar>
        )}

        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell align="left"></TableCell>
              <TableCell align="left" padding="checkbox"></TableCell>
              <TableCell align="left">DESCRIÇÃO</TableCell>
              <TableCell align="right">VALOR UNITÁRIO</TableCell>
              <TableCell align="center">QTDE.</TableCell>
              <TableCell align="right">VALOR FRETE</TableCell>
              <TableCell align="right">TOTAL</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {G_Venda.ITENS.map((row, index) => (
              <TableRow
                hover
                selected={G_PosicaoProdTableClick === index}
                key={index}
                style={{ height: '8vh', cursor: 'pointer' }}
                component={'tr'}
                onClick={() => {
                  const data = G_Venda.ITENS.filter(function (prod, indexprod) {
                    return indexprod === index
                  })
                  SetPosicaoProdTableClick(index)
                  SetSelectedItem(data)
                }}
              >
                <TableCell
                  style={{
                    borderLeft: row.ITEM_AV ? 'solid #6022a9' : 'solid #ff4600',
                  }}
                />
                <TableCell align="left" padding="checkbox">
                  <Checkbox
                    style={{ color: row.ITEM_AV ? '#6022a9' : '#ff4600' }}
                    name="checkProd"
                    onChange={(e) => handleChecked(e.target.checked, row.INDEX)}
                    checked={G_Venda.ITENS[row.INDEX].CHECKED}
                  />
                </TableCell>

                <TableCell align="left" component="th" scope="row">
                  {row.DESCRICAO}
                </TableCell>
                <TableCell align="right" component="th" scope="row">
                  {formatters.formatMoney(row.VALOR_UNITARIO)}
                </TableCell>
                <TableCell align="center" component="th" scope="row">
                  {row.QUANTIDADE}
                </TableCell>
                <TableCell align="right" component="th" scope="row">
                  {formatters.formatMoney(row.VALOR_FRETE)}
                </TableCell>
                <TableCell align="right" component="th" scope="row">
                  {formatters.formatMoney(row.VALOR_ITEM + row.VALOR_FRETE)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {G_Venda.QUANTIDADE_TOTAL === 0 && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: 'calc(100% - 105px)',
              flexDirection: 'column',
              opacity: 0.4,
            }}
          >
            <img src={EmptyCart} alt="Empty" />
            <div style={{ marginTop: 15 }}>Nenhum item encontrado</div>
          </div>
        )}
      </TableContainer>
      <Card
        style={{
          width: '100%',
          marginLeft: 10,
          height: '250px',
          marginTop: 20,
        }}
        component={Paper}
      >
        <AppBar position="static" style={{ backgroundColor: '#6022a9' }}>
          <Toolbar variant="dense">
            <Typography variant="h6" color="inherit">
              Resumo do Carrinho
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container spacing={3} style={{ padding: 20 }}>
          <Grid style={{ marginTop: 10 }} item xs={6}>
            <label style={{ marginTop: 100, fontSize: 16 }}>
              {G_Venda.QUANTIDADE_TOTAL} TOTAL <strong>ITENS</strong>
            </label>
          </Grid>

          <Grid style={{ marginTop: 10, textAlign: 'right' }} item xs={6}>
            <label style={{ marginRight: 10, fontSize: 16 }}>
              <strong>{formatters.formatMoney(G_Venda.VALOR_ITENS_AV + G_Venda.VALOR_ITENS_PL)}</strong>
            </label>
          </Grid>

          <Grid item xs={6}>
            <label style={{ marginTop: 100, fontSize: 16 }}>
              TOTAL <strong>FRETE</strong>
            </label>
          </Grid>

          <Grid style={{ textAlign: 'right' }} item xs={6}>
            <label style={{ marginRight: 10, fontSize: 16 }}>
              <strong>{formatters.formatMoney(G_Venda.VALOR_FRETE)}</strong>
            </label>
          </Grid>

          <Grid item xs={6}>
            <label style={{ marginTop: 100, fontSize: 16 }}>
              TOTAL <strong>DESCONTO</strong>
            </label>
          </Grid>

          <Grid style={{ textAlign: 'right' }} item xs={6}>
            <label style={{ marginRight: 10, fontSize: 16 }}>
              <strong> {formatters.formatMoney(G_Venda.VALOR_DESCONTO_AV + G_Venda.VALOR_DESCONTO_PL)}</strong>
            </label>
          </Grid>

          <Grid item xs={6}>
            <label style={{ marginTop: 100, fontSize: 20, fontWeight: 'bold' }}>TOTAL A PAGAR</label>
          </Grid>

          <Grid style={{ textAlign: 'right', fontWeight: 'bold' }} item xs={6}>
            <label style={{ marginRight: 10, fontSize: 20, color: '#3bd553' }}>
              <strong>{formatters.formatMoney(G_Venda.VALOR_VENDA_AV + G_Venda.VALOR_VENDA_PL)}</strong>
            </label>
          </Grid>
        </Grid>
      </Card>
    </>
  )
}

export default CustomTable

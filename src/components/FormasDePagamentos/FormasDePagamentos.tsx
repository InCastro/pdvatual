import React, { useEffect, useState } from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button, DialogContent, TextField } from '@material-ui/core'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import Grid from '@material-ui/core/Grid'
import { useAppContext } from '../../context/AppContexts'
import { useHistory } from 'react-router-dom'
import { formatters, round } from '../../utils'

export function FormasDePagamentos() {
  const {
    OpenFormasDePagamentos,
    SetOpenFormasDePagamentos,
    FormaDePagamento,
    SetFormaDePagamento,
    Pagamentos,
    SetPagamentos,
    G_Venda,
    Resta,
  } = useAppContext()

  const [result, Setresult] = useState('')
  const [parcelas, SetParcelas] = useState(1)

  const history = useHistory()

  useEffect(() => {
    Setresult(
      result ||
      formatters.formatMoney(
        round(G_Venda.VALOR_VENDA_PL + G_Venda.VALOR_VENDA_AV, 2)
      )
    )
  }, [result])

  useEffect(() => {
    Setresult(
      !Resta
        ? formatters.formatMoney(
          round(G_Venda.VALOR_VENDA_PL + G_Venda.VALOR_VENDA_AV, 2)
        )
        : formatters.formatMoney(Resta)
    )
  }, [Math.round(Resta)])

  const handleClose = () => {
    SetOpenFormasDePagamentos(false)
  }

  const formatarMoeda = (event: any) => {
    if (result === '') {
      var elemento: string = event.target.value
    } else {
      var elemento: string = event.target.value
    }
    let valor = elemento
    valor = valor + ''
    valor = valor.replace(/[\D]+/g, '')
    valor = valor + ''
    valor = valor.replace(/([0-9]{2})$/g, ',$1')
    if (valor.length > 6) {
      valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2')
    }
    if (valor.length === 3) {
      valor = 0 + valor
    }
    let number = valor.substring(1, 0)
    if (valor.length > 4 && parseInt(number) === 0) {
      valor = valor.substr(1, 14)
    }
    Setresult(`R$ ${valor}`)
  }

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    SetFormaDePagamento(event.target.value as number)
  }

  const handleGoCheckout = () => {
    history.push('/finalizar')
    SetOpenFormasDePagamentos(false)

    G_Venda.PAGAMENTO.push({
      COD_PAGAMENTO: methods[FormaDePagamento].id,
      DESCRICAO_PAGAMENTO: methods[FormaDePagamento].description,
      NSU_PAGAMENTO: '',
      NUM_PARCELA: FormaDePagamento === 1 ? parcelas : 1,
      VALOR_PAGAMENTO: parseFloat(result.replace('R$ ', '').replace('R$ ', '').replace('.', '').replace(',', '.'))
    })

    SetPagamentos([
      ...Pagamentos,
      {
        forma: methods[FormaDePagamento].description,
        valor: result,
        parcelas: FormaDePagamento === 1 ? parcelas : 1,
      },
    ])
  }

  const methods = [
    {
      id: 0,
      description: 'Dinheiro',
    },
    {
      id: 1,
      description: 'Crédito',
    },
    {
      id: 2,
      description: 'Débito',
    },
    {
      id: 3,
      description: 'Pix',
    },
    {
      id: 4,
      description: 'Boleto',
    },
  ]

  const installments = Array.from(Array(12).keys(), (n) => n + 1)

  return (
    <>
      <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={OpenFormasDePagamentos}>
        <DialogTitle style={{ width: 600 }} id="alert-dialog-title">
          Selecione a forma de pagamento
        </DialogTitle>
        <DialogContent>
          <Grid container xs={12} spacing={2} style={{ margin: 0 }}>
            <Grid item xs>
              <FormControl variant="outlined" style={{ width: '100%' }}>
                <InputLabel>Forma de Pagamento</InputLabel>
                <Select value={FormaDePagamento} autoFocus onChange={handleChange} label="Forma de Pagamento">
                  {methods.map((method) => (
                    <MenuItem value={method.id}>{method.description}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            {FormaDePagamento === 1 && (
              <Grid item xs={3} style={{ marginRight: 0 }}>
                <FormControl variant="outlined" style={{ width: '100%' }}>
                  <InputLabel>Parcelas</InputLabel>
                  <Select value={parcelas} onChange={(evt) => SetParcelas(Number(evt.target.value))} label="Parcelas">
                    {installments.map((i) => (
                      <MenuItem value={i}>{i}x</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
            )}
            <Grid item xs>
              <TextField
                error={
                  FormaDePagamento !== 0 &&
                  parseFloat(result.replace('R$ ', '').replace('R$ ', '').replace('.', '').replace(',', '.')) > Resta
                }
                style={{ width: '100%' }}
                id="outlined-size-normal"
                variant="outlined"
                placeholder="Valor R$"
                helperText={
                  FormaDePagamento !== 0 &&
                    parseFloat(result.replace('R$ ', '').replace('R$ ', '').replace('.', '').replace(',', '.')) > Resta
                    ? 'Maior que o total da venda'
                    : ''
                }
                onChange={formatarMoeda}
                type="text"
                inputProps={{
                  maxlength: 13,
                }}
                value={result}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button
            onClick={handleGoCheckout}
            disabled={
              FormaDePagamento !== 0 &&
              parseFloat(result.replace('R$ ', '').replace('R$ ', '').replace('.', '').replace(',', '.')) > Resta
            }
            color="primary"
          >
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
export default FormasDePagamentos

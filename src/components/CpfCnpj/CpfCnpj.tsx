import React, { useState, useCallback } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { useAppContext } from './../../context/AppContexts'
import UseKeyPress from '../../hooks/useKeyPress'
import { cpf } from 'cpf-cnpj-validator'
import axios from 'axios'
import { removeCharacter, UrlAPI } from './../../utils'
import Snackbar from '@material-ui/core/Snackbar'
import { Alert, AlertTitle } from '@material-ui/lab'
import { InputCpfCnpj } from './../InputCpfCnpj'

export function CpfCnpj() {
  const [OpenAlert, SetOpenAlert] = useState(false)

  const initKeyPress = () => {
    UseKeyPress('Escape', (key: string) => {
      if (OpenDialogCPF) {
        ; (document.getElementById('buttonCancelar') as HTMLButtonElement).click()
      }
    })

    UseKeyPress('Enter', (key: string) => {
      if (OpenDialogCPF) {
        ; (document.getElementById('inputCpf') as HTMLElement).focus()
      }
    })
  }

  initKeyPress()

  const {
    OpenDialogCPF,
    SetOpenDialogCPF,
    G_ListOrc,
    SetListOrc,
    G_CpfCnpj,
    SetValidateCpf,
    G_StatusVendaPl,
    SetLoading,
  } = useAppContext()
  const [CPF, setCPF] = useState('')
  const [ErrorCPF, setErrorCPF] = useState(false)
  const { SetOpenListaOrc } = useAppContext()

  const handleClose = () => {
    SetOpenDialogCPF(false)
  }

  const CloseAlert = () => {
    SetOpenAlert(false)
  }

  const validateCPF = () => {
    cpf.isValid(G_CpfCnpj) ? ListaOrcamento() : SetValidateCpf(true)
  }

  const ListaOrcamento = () => {
    getCpfCnpjAV(G_CpfCnpj)
    SetOpenDialogCPF(false)
    /* if (!G_StatusVendaPl) {
       
     } else {
       
     } */
  }

  const edicaoCPF = (value: string) => {
    setErrorCPF(false)
    setCPF(value)
  }

  const getCpfCnpjAV = useCallback(async (cpfCnpj: string) => {
    const method = 'get'
    const url = `${UrlAPI}consultaVenda/${removeCharacter(cpfCnpj)}`
    SetLoading(true)
    await axios[method](url).then((res) => {
      if (res.data.Orcamento.length !== 0) {
        SetListOrc(res.data.Orcamento)
        SetOpenListaOrc(true)
        SetOpenDialogCPF(false)
        SetLoading(false)
      } else {
        SetLoading(false)

        SetOpenAlert(true)
      }
    })
  }, [])

  return (
    <div>
      <Dialog open={OpenDialogCPF} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle style={{ width: 400 }} id="form-dialog-title">
          DIGITE CPF/CNPJ
        </DialogTitle>
        <DialogContent>
          <form onSubmit={validateCPF}>
            <InputCpfCnpj />
          </form>
        </DialogContent>
        <DialogActions>
          <Button id={'buttonCancelar'} onClick={handleClose} color="primary">
            Cancelar (ESC)
          </Button>
          <Button id={'buttonConfirmar'} onClick={() => validateCPF()} color="primary">
            Confirmar (ENTER)
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar open={OpenAlert} autoHideDuration={6000} onClose={CloseAlert}>
        <Alert onClose={CloseAlert} severity="info">
          <AlertTitle>info</AlertTitle>
          Não existe orçamento para este CPF/CNPJ.
        </Alert>
      </Snackbar>
    </div>
  )
}

export default CpfCnpj

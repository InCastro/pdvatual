import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button, Accordion, AccordionSummary, AccordionDetails, List, ListItem } from '@material-ui/core'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { useAppContext } from '../../context/AppContexts'
import { round } from '../../utils'

export function Orcamento() {
  const { OpenListaOrc, SetOpenListaOrc, ListVenda, SetStatusVendasAv, G_ListOrc, SetCpfCnpj, G_Venda, SetPosicaoProdTable, SetPosicaoProdTableClick, G_IdTurno } = useAppContext()

  const handleClose = () => {
    SetOpenListaOrc(false)
    SetCpfCnpj('')
  }

  const confirmarVenda = () => {
    SetStatusVendasAv(true)
    SetOpenListaOrc(false)
  }

  return (
    <>
      <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={OpenListaOrc}>
        <DialogTitle style={{ width: 600 }} id="alert-dialog-title">
          Lista de Orçamentos
        </DialogTitle>
        <List>
          {G_ListOrc.map((lista, index) => (
            <ListItem key={index}>
              <div>
                <Accordion style={{ width: 570 }}>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-label="Expand"
                    aria-controls="additional-actions1-content"
                    id="additional-actions1-header"
                  >
                    <FormControlLabel
                      aria-label="Acknowledge"
                      onClick={(event) => event.stopPropagation()}
                      onFocus={(event) => event.stopPropagation()}
                      control={
                        <Button
                          style={{ backgroundColor: '#6022a9', color: '#FFF', marginRight: 10 }}
                          onClick={function () {
                            lista.ITENS.map(itemOrc => {
                              G_Venda.ITENS.push({
                                DESCRICAO: itemOrc.DESCRICAO,
                                QUANTIDADE: itemOrc.QUANTIDADE,
                                SKU: itemOrc.SKU,
                                EAN: itemOrc.EAN,
                                VALOR_FRETE: itemOrc.VALOR_FRETE,
                                VALOR_UNITARIO: itemOrc.VALOR_UNITARIO,
                                DESCONTO: 0,
                                DESCONTO_RATEIO: 0,
                                VALOR_ITEM: round((itemOrc.VALOR_UNITARIO * itemOrc.QUANTIDADE) + itemOrc.VALOR_FRETE, 2),
                                ITEM_AV: true,
                                INDEX: G_Venda.ITENS.length,
                                CHECKED: false,
                                KEY_RESERVAPROD: ''

                              });
                              G_Venda.ID_TURNO = G_IdTurno;
                              G_Venda.QUANTIDADE_TOTAL = G_Venda.QUANTIDADE_TOTAL + itemOrc.QUANTIDADE;
                              G_Venda.VALOR_ITENS_AV = round(G_Venda.VALOR_ITENS_AV + (itemOrc.QUANTIDADE * itemOrc.VALOR_UNITARIO), 2);
                              G_Venda.VALOR_FRETE = G_Venda.VALOR_FRETE + itemOrc.VALOR_FRETE;
                              G_Venda.VALOR_VENDA_AV = round(G_Venda.VALOR_FRETE + G_Venda.VALOR_ITENS_AV - G_Venda.VALOR_DESCONTO_AV, 2);
                              SetPosicaoProdTable(G_Venda.ITENS.length)
                              SetPosicaoProdTableClick(G_Venda.ITENS.length)
                              G_Venda.COD_AV = lista.ID_ORCAMENTO;
                            })
                            confirmarVenda();
                          }}
                        >
                          Faturar
                        </Button>
                      }
                      label={
                        lista.ID_ORCAMENTO +
                        ` - ` +
                        lista.CLIENTE_NOME +
                        ` - ` +
                        lista.ITENS.reduce(function (itens, currentValue) {
                          return (
                            itens + (currentValue.QUANTIDADE * currentValue.VALOR_UNITARIO + currentValue.VALOR_FRETE)
                          )
                        }, 0).toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })
                      }
                      style={{ marginTop: 1 }}
                    />
                  </AccordionSummary>
                  {lista.ITENS.map((itens) => (
                    <AccordionDetails key={itens.SKU}>
                      <Typography color="textSecondary">
                        {`Descrição: ` + itens.DESCRICAO}
                        {<br />}
                        {`Prc. Unitário: ` +
                          itens.VALOR_UNITARIO.toLocaleString('pt-br', {
                            style: 'currency',
                            currency: 'BRL',
                          })}
                        {<br />}
                        {`Qtde.: ` + itens.QUANTIDADE}
                        {<br />}
                        {`Frete: ` +
                          itens.VALOR_FRETE.toLocaleString('pt-br', {
                            style: 'currency',
                            currency: 'BRL',
                          })}
                        {<br />}
                        {`TOTAL: ` +
                          (itens.QUANTIDADE * itens.VALOR_UNITARIO + itens.VALOR_FRETE).toLocaleString('pt-br', {
                            style: 'currency',
                            currency: 'BRL',
                          })}
                      </Typography>
                    </AccordionDetails>
                  ))}
                  <AccordionDetails>
                    <Typography color="textSecondary">
                      {`TOTAL ITENS: ` +
                        lista.ITENS.reduce(function (itens, currentValue) {
                          return itens + currentValue.QUANTIDADE * currentValue.VALOR_UNITARIO
                        }, 0).toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })}
                      {<br />}
                      {`TOTAL FRETE: ` +
                        lista.ITENS.reduce(function (itens, currentValue) {
                          return itens + currentValue.VALOR_FRETE
                        }, 0).toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })}
                      {<br />}
                      {`TOTAL VENDA: ` +
                        lista.ITENS.reduce(function (itens, currentValue) {
                          return (
                            itens + (currentValue.QUANTIDADE * currentValue.VALOR_UNITARIO + currentValue.VALOR_FRETE)
                          )
                        }, 0).toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })}
                    </Typography>
                  </AccordionDetails>
                </Accordion>
              </div>
            </ListItem>
          ))}
        </List>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
export default Orcamento

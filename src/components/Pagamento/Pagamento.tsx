import React, { useEffect, useState, useCallback } from 'react'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import { TextField, Divider } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import { CustomTable } from '../Table'
import styles from './Principal.module.css'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { useAppContext } from '../../context/AppContexts'
import UseKeyPress from '../../hooks/useKeyPress'
import { useHistory } from 'react-router-dom'
import { UrlAPI, round } from './../../utils'
import axios from 'axios'

import { formatters } from '../../utils'

export function Pagamento() {
  const [total, setTotal] = useState(0)
  const [pago, setPago] = useState(0)

  const history = useHistory()
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const {
    SelectedItem,
    SetListVenda,
    SetCpfCnpj,
    SetOpenFormasDePagamentos,
    G_StatusVendaAv,
    G_StatusVendaPl,
    Pagamentos,
    SetStatusVendasPl,
    SetStatusVendasAv,
    SetListOrc,
    Resta,
    SetResta,
    SetPagamentos,
    SetLoading,
    G_Venda,
    SetDescMontagemCarrinho,
    SetDescFinalizarVenda
  } = useAppContext()

  const initKeyPress = () => {
    UseKeyPress('F10', (key: string) => {
      ; (document.getElementById('buttonPagar') as HTMLButtonElement).click()
    })

    UseKeyPress('Enter', (key: string) => {
      if (G_StatusVendaAv && G_StatusVendaPl) {
        alert(`you pressed ${key}!`)
      }
    })
  }

  initKeyPress()

  useEffect(() => {
    setTimeout(() => {
      SetLoading(false)
      SetDescMontagemCarrinho(false);
      SetDescFinalizarVenda(true);
    }, 1000)

    let total = round(G_Venda.VALOR_VENDA_AV + G_Venda.VALOR_VENDA_PL, 2);

    let pago = Pagamentos.reduce(function (itens: any, currentValue: { valor: any }) {
      return (
        itens + parseFloat(currentValue.valor.replace('R$ ', '').replace('R$ ', '').replace('.', '').replace(',', '.'))
      )
    }, 0)

    setTotal(total)

    setPago(pago)

    SetResta(total - pago)
  }, [G_Venda, Pagamentos])

  const limparVariaveis = () => {
    SetListVenda([])
    SetStatusVendasAv(false)
    SetStatusVendasPl(false)
    SetCpfCnpj('')
    SetListOrc([])
    SetPagamentos([])
    G_Venda.ID_VENDA = 0;
    G_Venda.ITENS = [];
    G_Venda.PAGAMENTO = [];
    G_Venda.QUANTIDADE_TOTAL = 0;
    G_Venda.VALOR_DESCONTO_AV = 0;
    G_Venda.VALOR_DESCONTO_PL = 0;
    G_Venda.VALOR_FRETE = 0;
    G_Venda.VALOR_ITENS_AV = 0;
    G_Venda.VALOR_ITENS_PL = 0;
    G_Venda.VALOR_VENDA_AV = 0;
    G_Venda.VALOR_VENDA_PL = 0;
    G_Venda.VALOR_TOTAL = 0;

    history.push('/')
  }


  const FinalizarVenda = useCallback(async () => {
    const method = 'post'
    const url = `${UrlAPI}finalizarVenda`
    await axios[method](url, G_Venda).then((res) => {
      limparVariaveis();
    }).catch((res) => {
      console.log(res.data)
    }
    )
  }, [])

  return (
    <>
      <div style={{ flexGrow: 1 }}>
        <Grid>
          <Grid item xs={12} className={styles.Grid}>
            <Card style={{ backgroundColor: '#FAFAFA' }} className={styles.Card1} variant="outlined" square>
              <br />
              <Grid container spacing={3} style={{ height: '100%', maxHeight: '100%' }}>
                <Grid item xs={7} style={{ height: '100%', maxHeight: '100%' }}>
                  <CustomTable />
                </Grid>
                <Grid item xs={5}>
                  <Card style={{ marginRight: 10, height: 'calc(100% - 105px)' }}>
                    <Grid container spacing={1}>
                      <Grid item xs={12}>
                        <Card
                          style={{
                            height: 150,
                            backgroundColor: '#6022a9',
                            padding: 20,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                          variant="outlined"
                        >
                          <Grid container>
                            {/* {getItem().SKU && ( */}
                            <Grid item xs={6}>
                              <h4
                                style={{
                                  color: '#ffffff',
                                  fontSize: 10,
                                  marginBottom: 5,
                                }}
                              >
                                Total a Pagar
                              </h4>
                              <h4
                                style={{
                                  color: '#ffffff',
                                  fontSize: 28,
                                  fontWeight: 500,
                                }}
                              >
                                {formatters.formatMoney(total)}
                              </h4>
                            </Grid>
                            <Grid item xs={6}>
                              <h4
                                style={{
                                  color: '#ffffff',
                                  fontSize: 10,
                                  marginBottom: 5,
                                }}
                              >
                                {Resta < 0 ? 'Troco' : 'Resta pagar'}
                              </h4>
                              <h4
                                style={{
                                  color: '#ffffff',
                                  fontSize: 28,
                                  fontWeight: 500,
                                }}
                              >
                                {Resta < 0 ? formatters.formatMoney(Resta * -1) : formatters.formatMoney(Resta)}
                              </h4>
                            </Grid>
                          </Grid>
                        </Card>
                      </Grid>

                      <Grid item xs={12}>
                        <AppBar position="static" style={{ background: '#ffffff' }}>
                          <Toolbar variant="dense">
                            <Typography variant="h6" style={{ color: '#000000' }}>
                              Resumo do Pagamento
                            </Typography>
                          </Toolbar>
                        </AppBar>
                        <Grid container spacing={1} style={{ padding: 20 }}>
                          {Pagamentos.map(
                            (
                              item: {
                                forma:
                                | boolean
                                | React.ReactChild
                                | React.ReactFragment
                                | React.ReactPortal
                                | null
                                | undefined
                                valor: any
                                parcelas: any
                              },
                              index: any
                            ) => {
                              return (
                                <React.Fragment key={index}>
                                  <Grid style={{ marginTop: 10 }} item xs={4}>
                                    <label style={{ fontSize: 16 }}>{item.forma}</label>
                                  </Grid>

                                  <Grid style={{ marginTop: 10, textAlign: 'right' }} item xs={4}>
                                    <label style={{ fontSize: 16 }}>
                                      {item.parcelas === 1 ? (
                                        'À vista'
                                      ) : (
                                        <>
                                          <strong style={{ fontSize: 12 }}>{`${item.parcelas}x`}</strong>{' '}
                                          <span>{`${formatters.formatMoney(
                                            item.valor
                                              .replace('R$ ', '')
                                              .replace('R$ ', '')
                                              .replace('.', '')
                                              .replace(',', '.') / item.parcelas
                                          )}`}</span>
                                        </>
                                      )}
                                    </label>
                                  </Grid>

                                  <Grid style={{ marginTop: 10, textAlign: 'right' }} item xs={4}>
                                    <label style={{ fontSize: 16 }}>{item.valor}</label>
                                  </Grid>
                                </React.Fragment>
                              )
                            }
                          )}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Card>
                  <Divider style={{ marginRight: 10 }} />
                  <Grid>
                    <Card style={{ marginRight: 10, height: '95px', padding: 20 }}>
                      <Grid container spacing={1} style={{ height: '100%', margin: 0 }}>
                        <Grid item xs={6} style={{ height: '100%' }}>
                          <Button
                            id={'buttonPagar'}
                            className="actions"
                            disabled={G_Venda.QUANTIDADE_TOTAL === 0}
                            onClick={() => {
                              if (total - pago === 0) {
                                FinalizarVenda();
                              } else {
                                SetOpenFormasDePagamentos(true)
                              }
                            }}
                            style={{
                              backgroundColor: '#3bd553',
                              color: '#FFF',
                              width: '100%',
                              height: '100%',
                            }}
                          >
                            {Math.abs(total - pago) === 0 ? 'FINALIZAR VENDA' : 'PAGAR AGORA (F10)'}
                          </Button>
                        </Grid>
                        <Grid item xs={6} style={{ height: '100%' }}>
                          <Button
                            id={'buttonPagar'}
                            className="actions"
                            onClick={() => {
                              const dataAV = G_Venda.ITENS.filter(function (prod) {
                                return prod.ITEM_AV === true
                              })

                              G_Venda.VALOR_ITENS_AV = round(dataAV.reduce(function (itens, currentValue) {
                                return ( // Quando cancela, considera apenas o desconto do unitario
                                  itens +
                                  (currentValue.VALOR_UNITARIO * currentValue.QUANTIDADE)
                                )
                              }, 0), 2)

                              const dataPL = G_Venda.ITENS.filter(function (prod) {
                                return prod.ITEM_AV === false
                              })

                              G_Venda.VALOR_ITENS_PL = round(dataPL.reduce(function (itens, currentValue) {
                                return ( // Quando cancela, considera apenas o desconto do unitario
                                  itens +
                                  (currentValue.VALOR_UNITARIO * currentValue.QUANTIDADE)
                                )
                              }, 0), 2)

                              G_Venda.VALOR_DESCONTO_AV = dataAV.reduce(function (itens, currentValue) {
                                return ( // Quando cancela, considera apenas o desconto do unitario
                                  itens +
                                  (currentValue.DESCONTO)
                                )
                              }, 0)

                              G_Venda.VALOR_DESCONTO_PL = dataPL.reduce(function (itens, currentValue) {
                                return ( // Quando cancela, considera apenas o desconto do unitario
                                  itens +
                                  (currentValue.DESCONTO)
                                )
                              }, 0)

                              G_Venda.VALOR_VENDA_AV = round((G_Venda.VALOR_ITENS_AV - G_Venda.VALOR_DESCONTO_AV), 2);
                              G_Venda.VALOR_VENDA_PL = round((G_Venda.VALOR_ITENS_PL - G_Venda.VALOR_DESCONTO_PL), 2);
                              SetPagamentos([])
                              history.push('/')
                            }}
                            style={{
                              backgroundColor: '#efefef',
                              color: '#555',
                              width: '100%',
                              height: '100%',
                            }}
                          >
                            CANCELAR
                          </Button>
                        </Grid>
                      </Grid>
                    </Card>
                  </Grid>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </div>
    </>
  )
}

export default Pagamento
function SetListVenda(arg0: never[]) {
  throw new Error('Function not implemented.')
}

function SetCpfCnpj(arg0: string) {
  throw new Error('Function not implemented.')
}

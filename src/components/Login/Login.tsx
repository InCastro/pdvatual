import React, { useReducer, useEffect, useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import LogoImg from '../../assets/mobly_logo.png'
import { useAppContext } from '../../context/AppContexts'
import { useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import { UrlAPI } from '../../utils';
import axios from 'axios';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      width: 400,
      margin: `${theme.spacing(0)} auto`
    },
    loginBtn: {
      marginTop: theme.spacing(2),
      flexGrow: 1,
      background: '#6022a9',
      "&:hover": {
        backgroundColor: '#ff4600'
      }
    },
    header: {
      textAlign: 'center',
      background: '#6022a9',
      color: '#fff',
      borderBottom: '10px solid #ff4600',

    },
    card: {
      marginTop: theme.spacing(10)
    }
  })
);

//state type

type State = {
  username: string
  password: string
  isButtonDisabled: boolean
  helperText: string
  isError: boolean
};

const initialState: State = {
  username: '',
  password: '',
  isButtonDisabled: true,
  helperText: '',
  isError: false
};

type Action = { type: 'setUsername', payload: string }
  | { type: 'setPassword', payload: string }
  | { type: 'setIsButtonDisabled', payload: boolean }
  | { type: 'loginSuccess', payload: string }
  | { type: 'loginFailed', payload: string }
  | { type: 'setIsError', payload: boolean };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'setUsername':
      return {
        ...state,
        username: action.payload
      };
    case 'setPassword':
      return {
        ...state,
        password: action.payload
      };
    case 'setIsButtonDisabled':
      return {
        ...state,
        isButtonDisabled: action.payload
      };
    case 'loginSuccess':
      return {
        ...state,
        helperText: action.payload,
        isError: false
      };
    case 'loginFailed':
      return {
        ...state,
        helperText: action.payload,
        isError: true
      };
    case 'setIsError':
      return {
        ...state,
        isError: action.payload
      };
  }
}

export const Login = () => {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);
  const {
    SetLoggedIn,
    SetLoading,
    LoggedIn,
    G_DadosConfigPdv,
    SetGIdTurno
  } = useAppContext()
  const history = useHistory()

  type SangriaSuprimento = typeof initialSangriaSuprimento;

  const initialSangriaSuprimento =
  {
    ID_TURNO: 0,
    DESCRICAO_EMPRESA: G_DadosConfigPdv.FANTASIA,
    CNPJ_EMPRESA: G_DadosConfigPdv.CNPJ_EMPRESA,
    DESCRICAO_RETIRADA: 'Fundo de Troco',
    ENDERECO_EMPRESA: G_DadosConfigPdv.ENDERECO_EMPRESA,
    FONE_EMPRESA: G_DadosConfigPdv.FONE_EMPRESA,
    FONTSIZE: 8,
    ID_CAIXA: G_DadosConfigPdv.NUM_CAIXA,
    NUM_CAIXA: G_DadosConfigPdv.NUM_CAIXA,
    TIPO_COMPROVANTE: 'SUPRIMENTO',
    VALOR_COMPROVANTE: 'R$ 0,00'
  }

  const [result, Setresult] = useState('')
  const [suprimento, SetSuprimento] = useState<SangriaSuprimento>(initialSangriaSuprimento)

  useEffect(() => {
    SetLoading(false)
    if (state.username.trim() && state.password.trim()) {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: false
      });
    } else {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: true
      });
    }
  }, [state.username, state.password]);

  const handleLogin = () => {
    if (state.username === 'operador@mobly.com.br' && state.password === 'operador') {
      SetLoading(true)
      SetLoggedIn(true)
      history.push('./')
      dispatch({
        type: 'loginSuccess',
        payload: 'Login efetuado com sucesso!'
      });
    } else {
      dispatch({
        type: 'loginFailed',
        payload: 'Usuário ou senha inválidos'
      });
    }
  };

  const handleKeyPress = (event: React.KeyboardEvent) => {
    if (event.keyCode === 13 || event.which === 13) {
      state.isButtonDisabled || handleLogin();
    }
  };

  const handleUsernameChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setUsername',
        payload: event.target.value
      });
    };

  const handlePasswordChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setPassword',
        payload: event.target.value
      });
    }

  const formatarMoeda = (event: any) => {
    let valor: string = event.target.value
    valor = valor + ''
    valor = valor.replace(/[\D]+/g, '')
    valor = valor + ''
    valor = valor.replace(/([0-9]{2})$/g, ',$1')
    if (valor.length > 6) {
      valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2')
    }
    if (valor.length === 3) {
      valor = 0 + valor
    }
    let number = valor.substring(1, 0)
    if (valor.length > 4 && parseInt(number) === 0) {
      valor = valor.substr(1, 14)
    }
    Setresult(`R$ ${valor}`)
    suprimento.VALOR_COMPROVANTE = (`R$ ${valor}`);
  }

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })

  const AbrirCaixa = () => {
    swalWithBootstrapButtons.fire({
      title: 'Abrir Caixa?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        if (state.username === 'operador@mobly.com.br' && state.password === 'operador') {
          SetLoading(true)
          const method = 'post';
          const url = `${UrlAPI}fundoDeTroco`;
          axios[method](url, suprimento)
            .then((response) => {
              SetGIdTurno(response.data.response);
            })
          setTimeout(() => {
            SetLoading(false)
            history.push('./')
          }, 1000)
          dispatch({
            type: 'loginSuccess',
            payload: 'Login efetuado com sucesso!'
          });
        } else {
          dispatch({
            type: 'loginFailed',
            payload: 'Usuário ou senha inválidos'
          });
        }
      }
    })
  }
  return (
    <>
      <div style={{ height: '82vh' }}>
        <form className={classes.container} noValidate autoComplete="off">
          <Card className={classes.card}>
            <CardHeader className={classes.header} title={<img style={{ marginTop: 10 }} src={LogoImg} alt="Mobly" />} />
            <CardContent>
              <div>
                <TextField
                  error={state.isError}
                  fullWidth
                  id="username"
                  type="email"
                  label="Operador"
                  placeholder="Operador"
                  margin="normal"
                  onChange={handleUsernameChange}
                  onKeyPress={handleKeyPress}
                />
                <TextField
                  error={state.isError}
                  fullWidth
                  id="password"
                  type="password"
                  label="Senha"
                  placeholder="Senha"
                  margin="normal"
                  helperText={state.helperText}
                  onChange={handlePasswordChange}
                  onKeyPress={handleKeyPress}
                />
                <TextField
                  fullWidth
                  id="fundotroco"
                  label="Fundo de Troco"
                  placeholder="Fundo de Troco"
                  margin="normal"
                  helperText={state.helperText}
                  onChange={formatarMoeda}
                  value={result}
                />
              </div>
            </CardContent>
            <CardActions>
              <Button
                variant="contained"
                size="large"
                color="primary"
                className={classes.loginBtn}
                onClick={AbrirCaixa}
                disabled={state.isButtonDisabled}>
                Entrar
              </Button>
            </CardActions>
          </Card>
        </form>
      </div>
    </>

  );
}

export default Login;
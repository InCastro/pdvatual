import React, { useEffect, useState, useCallback } from 'react'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Dialog from '@material-ui/core/Dialog'
import { Button, TextField } from '@material-ui/core'
import { useAppContext } from './../../context/AppContexts'
import UseKeyPress from '../../hooks/useKeyPress'
import { kMaxLength } from 'buffer'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import axios from 'axios'
import Swal from 'sweetalert2'

interface ITipo {
  Tipo: string
}

export function SangriaSuprimento({ Tipo }: ITipo) {
  const { OpenDialogSangria, FormaDePagamento, SetOpenDialogSangria, SetFormaDePagamento,
    SetValorSanSup, G_ValorSanSup, G_DadosConfigPdv, G_IdTurno } = useAppContext()

  type SangriaSuprimento = typeof initialSangriaSuprimento;

  const initialSangriaSuprimento =
  {
    ID_TURNO: G_IdTurno,
    DESCRICAO_EMPRESA: G_DadosConfigPdv.FANTASIA,
    CNPJ_EMPRESA: G_DadosConfigPdv.CNPJ_EMPRESA,
    DESCRICAO_RETIRADA: 'Ñ Preencheu descrição',
    ENDERECO_EMPRESA: G_DadosConfigPdv.ENDERECO_EMPRESA,
    FONE_EMPRESA: G_DadosConfigPdv.FONE_EMPRESA,
    FONTSIZE: 8,
    ID_CAIXA: G_DadosConfigPdv.NUM_CAIXA,
    NUM_CAIXA: G_DadosConfigPdv.NUM_CAIXA,
    TIPO_COMPROVANTE: 'S A N G R I A Pegar o Tipo',
    VALOR_COMPROVANTE: 'R$ 0,00'
  }

  const initKeyPress = () => {
    UseKeyPress('Escape', (key: string) => {
      handleClose()
    })

    UseKeyPress('PageDown', (key: string) => {
      alert(`you pressed ${key}!`)
    })
  }

  initKeyPress()

  const [Calculator, SetCalculator] = useState()
  const [teste, Setteste] = useState('')
  const [sangria, SetSangria] = useState<SangriaSuprimento>(initialSangriaSuprimento)

  const defaultMaskOptions = {
    prefix: '$',
    suffix: '',
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ',',
    allowDecimal: true,
    decimalSymbol: '.',
    decimalLimit: 2, // how many digits allowed after the decimal
    integerLimit: 7, // limit length of integer numbers
    allowNegative: false,
    allowLeadingZeroes: false,
  }

  const methods = [
    {
      id: 1,
      description: 'Dinheiro',
    },
  ]

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    SetFormaDePagamento(event.target.value as number)
  }

  const handleClose = () => {
    SetOpenDialogSangria(false)
  }

  function TipoSanSup(value: string) {
    if (value === 'SAN') {
      sangria.TIPO_COMPROVANTE = 'SANGRIA'
      return 'SANGRIA DE CAIXA'
    } else {
      sangria.TIPO_COMPROVANTE = 'SUPRIMENTO'
      return 'SUPRIMENTO DE CAIXA'
    }
  }

  const formatarMoeda = (event: any) => {
    let valor: string = event.target.value
    valor = valor + ''
    valor = valor.replace(/[\D]+/g, '')
    valor = valor + ''
    valor = valor.replace(/([0-9]{2})$/g, ',$1')
    if (valor.length > 6) {
      valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2')
    }
    if (valor.length === 3) {
      valor = 0 + valor
    }
    let number = valor.substring(1, 0)
    if (valor.length > 4 && parseInt(number) === 0) {
      valor = valor.substr(1, 14)
    }
    SetValorSanSup(`R$ ${valor}`)
    sangria.VALOR_COMPROVANTE = (`R$ ${valor}`);
    SetSangria({
      ...sangria,
      ID_TURNO: G_IdTurno,
      DESCRICAO_EMPRESA: G_DadosConfigPdv.FANTASIA,
      CNPJ_EMPRESA: G_DadosConfigPdv.CNPJ_EMPRESA,
      ENDERECO_EMPRESA: G_DadosConfigPdv.ENDERECO_EMPRESA,
      FONE_EMPRESA: G_DadosConfigPdv.FONE_EMPRESA,
      FONTSIZE: 8,
      ID_CAIXA: G_DadosConfigPdv.NUM_CAIXA,
      NUM_CAIXA: G_DadosConfigPdv.NUM_CAIXA,
    })
  }

  const handleKeyPress = (event: any) => {
    SetValorSanSup(event.key)
  }


  const postSangriaSuprimento = () => {

    const method = 'post';
    const url = `http://localhost:3333/sangriaSuprimento/`;
    axios[method](url, sangria)
      .then((res) => {
        SetOpenDialogSangria(false)
        Swal.fire({
          icon: 'success',
          text: 'Sangria realizada com sucesso.',
        })
      })
  };

  return (
    <div>
      <Dialog aria-labelledby="confirmation-dialog-title" open={OpenDialogSangria}>
        <DialogTitle id="confirmation-dialog-title">{TipoSanSup(Tipo)}</DialogTitle>
        <DialogContent style={{ height: 500, width: 500 }} dividers>
          <FormControl variant="outlined" style={{ width: '100%', marginTop: '5%' }}>
            <InputLabel>Forma de Pagamento</InputLabel>
            <Select value={FormaDePagamento} autoFocus onChange={handleChange} label="Forma de Pagamento">
              {methods.map((method) => (
                <MenuItem value={method.id}>{method.description}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <br />
          <br />
          <TextField
            style={{ width: '100%' }}
            id="outlined-size-normal"
            variant="outlined"
            placeholder="Valor R$"
            onChange={formatarMoeda}
            type="text"
            inputProps={{
              maxlength: 13,
            }}
            value={G_ValorSanSup}
          />
          <br />
          <br />
          <TextField
            style={{ width: '100%' }}
            id="outlined-multiline-static"
            label="Histórico"
            multiline
            rows={4}
            variant="outlined"
            onChange={(e) => { sangria.DESCRICAO_RETIRADA = e.target.value }}
          />
        </DialogContent>

        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            CANCELAR (ESC)
          </Button>
          <Button onClick={() => postSangriaSuprimento()} color="primary">
            SALVAR (PGDOWN)
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default SangriaSuprimento

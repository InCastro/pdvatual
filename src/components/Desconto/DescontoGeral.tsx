import React, { useState } from 'react';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { useAppContext } from "../../context/AppContexts";
import { TextField, Button } from '@material-ui/core';
import UseKeyPress from "../../hooks/useKeyPress";
import { formatters, round } from "../../utils";

export const DescontoGeral = () => {
    const initKeyPress = () => {
        UseKeyPress("Escape", (key: string) => {
            handleClose();
        });

        UseKeyPress("PageDown", (key: string) => {
            if (OpenDialogDesconto) {
                alert(`you pressed ${key}!`);
            }
        });
    };

    initKeyPress();

    const formatarMoeda = (event: any) => {
        let percentual: number;
        let valor: string = event.target.value
        valor = valor + ''
        valor = valor.replace(/[\D]+/g, '')
        valor = valor + ''
        valor = valor.replace(/([0-9]{2})$/g, ',$1')
        if (valor.length > 6) {
            valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2')
        }
        if (valor.length === 3) {
            valor = 0 + valor
        }
        let number = valor.substring(1, 0)
        if (valor.length > 4 && parseInt(number) === 0) {
            valor = valor.substr(1, 14)
        }
        SetValorDesc(`R$ ${valor}`)
        if (G_DescMontagemCarrinho) {  // Todo: regra de desconto na montagem do carrinho - 
            G_Venda.ITENS[G_PosicaoProdTableClick].DESCONTO = parseFloat(valor.replace('.', '').replace(',', '.'));
            percentual = round((G_Venda.ITENS[G_PosicaoProdTableClick].DESCONTO / G_Venda.ITENS[G_PosicaoProdTableClick].VALOR_ITEM) * 100, 2)
            SetPercentualDesc(`% ${percentual.toString().replace('.', ',')}`)
        } else { // Todo: regra de desconto na finaliza��o da venda. P.S.: os descontos v�o para os totais do 'cabecalho', quando salva que rateia entre os itens.
            //  G_Venda.VALOR_TOTAL_DESCONTO = parseFloat(valor.replace('.', '').replace(',', '.'));
            ///    percentual = round((parseFloat(valor.replace('.', '').replace(',', '.')) / G_Venda.VALOR_TOTAL_ITENS) * 100, 2)
        }

    }

    const formatarPorcentagem = (event: any) => {
        let percentual: number;
        let valor: string = event.target.value
        valor = valor + ''
        valor = valor.replace(/[\D]+/g, '')
        valor = valor + ''
        valor = valor.replace(/([0-9]{2})$/g, ',$1')
        if (valor.length > 6) {
            valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2')
        }
        if (valor.length === 3) {
            valor = 0 + valor
        }
        let number = valor.substring(1, 0)
        if (valor.length > 4 && parseInt(number) === 0) {
            valor = valor.substr(1, 14)
        }
        SetPercentualDesc(`% ${valor}`)

        if (G_DescMontagemCarrinho) {// Todo: regra de desconto na montagem do carrinho - 
            percentual = parseFloat(valor.replace(',', '.'));
            // G_Venda.ITENS[G_PosicaoProdTableClick].ITEM_AV ?
            G_Venda.ITENS[G_PosicaoProdTableClick].DESCONTO = round((percentual / 100) * G_Venda.ITENS[G_PosicaoProdTableClick].VALOR_ITEM, 2);
            SetValorDesc(formatters.formatMoney(round(G_Venda.ITENS[G_PosicaoProdTableClick].DESCONTO, 2)))
        } else {// Todo: regra de desconto na finaliza��o da venda. P.S.: os descontos v�o para os totais do 'cabecalho', quando salva que rateia entre os itens.
            // percentual = parseFloat(valor.replace(',', '.'));
            // G_Venda.VALOR_TOTAL_DESCONTO = round((percentual / 100) * G_Venda.VALOR_TOTAL_VENDA, 2);
            // SetValorDesc(formatters.formatMoney(round(G_Venda.VALOR_DESCONTO_AV, 2))) 
        }

    }

    const { OpenDialogDesconto, SetOpenDialogDesconto, G_Venda, G_PosicaoProdTableClick, G_DescFinalizarVenda, G_DescMontagemCarrinho, G_ValorDesc, SetValorDesc,
        G_PercentualDesc, SetPercentualDesc, SetGTotalIteChecked, G_TotalIteChecked, SetResta } =
        useAppContext();


    const saveDesc = () => {
        if (G_DescMontagemCarrinho) { // Carrinho Unitario
            if (G_ValorDesc.trim() === '') {
                G_Venda.VALOR_DESCONTO_AV = G_Venda.VALOR_DESCONTO_AV + 0;
                G_Venda.VALOR_VENDA_AV = round(G_Venda.VALOR_VENDA_AV - 0, 2);

                G_Venda.VALOR_DESCONTO_PL = G_Venda.VALOR_DESCONTO_PL + 0;
                G_Venda.VALOR_VENDA_PL = round(G_Venda.VALOR_VENDA_PL - 0, 2);
            } else {
                G_Venda.ITENS[G_PosicaoProdTableClick].ITEM_AV ?
                    G_Venda.VALOR_DESCONTO_AV = G_Venda.VALOR_DESCONTO_AV + parseFloat(G_ValorDesc.replace('R$ ', '').replace('R$', '').replace('.', '').replace(',', '.')) :
                    G_Venda.VALOR_DESCONTO_PL = G_Venda.VALOR_DESCONTO_PL + parseFloat(G_ValorDesc.replace('R$ ', '').replace('R$', '').replace('.', '').replace(',', '.'));

                G_Venda.ITENS[G_PosicaoProdTableClick].ITEM_AV ?
                    G_Venda.VALOR_VENDA_AV = round(G_Venda.VALOR_VENDA_AV - parseFloat(G_ValorDesc.replace('R$ ', '').replace('R$', '').replace('.', '').replace(',', '.')), 2) :
                    G_Venda.VALOR_VENDA_PL = round(G_Venda.VALOR_VENDA_PL - parseFloat(G_ValorDesc.replace('R$ ', '').replace('R$', '').replace('.', '').replace(',', '.')), 2)
            }
            G_Venda.ITENS[G_PosicaoProdTableClick].VALOR_ITEM = G_Venda.ITENS[G_PosicaoProdTableClick].VALOR_ITEM - G_Venda.ITENS[G_PosicaoProdTableClick].DESCONTO;

        }
        /*  else {  // Carrinho Global
              if (G_ValorDesc.trim() === '') {
                  G_Venda.VALOR_TOTAL_DESCONTO = G_Venda.VALOR_TOTAL_DESCONTO + 0;
                  G_Venda.VALOR_TOTAL_VENDA = round(G_Venda.VALOR_TOTAL_VENDA - G_Venda.VALOR_TOTAL_DESCONTO, 2);
              } else {
                  G_Venda.VALOR_TOTAL_DESCONTO = G_Venda.VALOR_TOTAL_DESCONTO + parseFloat(G_ValorDesc.replace('R$ ', '').replace('R$', '').replace('.', '').replace(',', '.'));
                  G_Venda.VALOR_TOTAL_VENDA = round(G_Venda.VALOR_TOTAL_VENDA - parseFloat(G_ValorDesc.replace('R$ ', '').replace('R$', '').replace('.', '').replace(',', '.')), 2);
              }
              SetResta(G_Venda.VALOR_TOTAL)
          } */


        SetOpenDialogDesconto(false);
    }

    const handleClose = () => {
        /*    if (G_DescMontagemCarrinho) {
               if (G_ValorDesc === '') {
                   SetOpenDialogDesconto(false);
               } else {
                   G_Venda.ITENS[G_PosicaoProdTableClick].DESCONTO = 0;
                   G_Venda.VALOR_TOTAL_DESCONTO = G_Venda.VALOR_TOTAL_DESCONTO - parseFloat(G_ValorDesc.replace('.', '').replace(',', '.'));
                   SetOpenDialogDesconto(false);
               }
           }
   
           if (G_DescFinalizarVenda) {
               if (G_ValorDesc === '') {
                   SetOpenDialogDesconto(false);
               } else {
                   G_Venda.VALOR_TOTAL_DESCONTO = G_Venda.VALOR_TOTAL_DESCONTO - parseFloat(G_ValorDesc.replace('.', '').replace(',', '.'));
                   SetOpenDialogDesconto(false);
               }
           } */
    }

    const DescValorUnitario = () => {
        return G_Venda.ITENS?.filter((item) => item)[G_PosicaoProdTableClick] || [];
    }

    return (
        <>
            <Dialog
                aria-labelledby="confirmation-dialog-title"
                open={OpenDialogDesconto}
            >
                <DialogTitle id="confirmation-dialog-title">
                    Desconto
                </DialogTitle>
                <DialogContent style={{ height: 300, width: 500 }} dividers>
                    <TextField
                        style={{ width: "100%" }}
                        id="outlined-size-normal"
                        variant="outlined"
                        placeholder="Valor R$"
                        onChange={formatarMoeda}
                        inputProps={{
                            maxlength: 13,
                        }}
                        value={G_ValorDesc}
                    />
                    <br />
                    <br />
                    <TextField
                        style={{ width: "100%" }}
                        id="outlined-size-normal"
                        variant="outlined"
                        placeholder="Percentual %"
                        autoFocus
                        inputProps={{
                            maxlength: 7,
                        }}
                        onChange={formatarPorcentagem}
                        value={G_PercentualDesc}
                    />
                    <br />
                    <br />
                    <label>TOTAL ITENS: <strong>
                        {G_DescMontagemCarrinho ? formatters.formatMoney((DescValorUnitario().QUANTIDADE * DescValorUnitario().VALOR_UNITARIO) - DescValorUnitario().DESCONTO)
                            : formatters.formatMoney(round(G_Venda.VALOR_VENDA_AV + G_Venda.VALOR_VENDA_PL, 2))}
                    </strong></label>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        CANCELAR (ESC)
                    </Button>
                    <Button onClick={saveDesc} color="primary">
                        SALVAR (PGDOWN)
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    )
}

export default DescontoGeral;
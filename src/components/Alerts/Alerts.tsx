import React, { useState } from 'react'
import styles from './Alerts.module.css'
import Icon from '@material-ui/core/Icon'

interface IAlert {
  message: string
  type: string
  isOpen: boolean
}

export const Alerts = ({ message, type, isOpen }: IAlert) => {
  const [open, setOpen] = useState(isOpen || false)
  const handleClose = () => {
    setOpen(false)
  }

  const types: { [key: string]: { background: string; color: string; icon: string } } = {
    success: {
      color: '#21a872',
      background: '#e5fff0',
      icon: 'check',
    },
    error: {
      color: '#ff0000',
      background: '#ffe3e3',
      icon: 'close',
    },
    alert: {
      color: '#007bff',
      background: '#e3f1ff',
      icon: 'priority_high',
    },
  }

  return (
    <>
      {open && (
        <div className={styles.Modal}>
          <div className={styles.overlay} onClick={handleClose}></div>
          <div className={styles.ModalContent}>
            <div
              style={{
                padding: 5,
                background: types[type].background,
                width: 50,
                height: 50,
                borderRadius: 100,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Icon style={{ color: types[type].color, fontWeight: 'bold', fontFamily: 'Material Icons' }}>
                {types[type].icon}
              </Icon>
            </div>
            <div className={styles.message}>{message}</div>
          </div>
        </div>
      )}
    </>
  )
}

export default Alerts

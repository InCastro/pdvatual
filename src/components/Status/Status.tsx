import Grid from '@material-ui/core/Grid'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import { useAppContext } from './../../context/AppContexts'

export function Status() {
  const { G_DadosConfigPdv } = useAppContext()

  function Format(timestamp: string | number | Date, lang: string | string[] | undefined, tz: string) {
    let dateObj = new Date(timestamp)

    return dateObj.toLocaleString(lang, {
      timeZone: tz,
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
    })
  }
  return (
    <Grid item xs={12}>
      <AppBar
        style={{
          height: 64,
          backgroundColor: '#6022a9',
        }}
        position="static"
      >
        <Toolbar>
          <Grid container spacing={1}>
            <Grid item xs={3} style={{ textAlign: 'center' }}>
              <div style={{ fontSize: 12, marginBottom: 8 }}>Loja</div>
              <div style={{ fontSize: 14, fontWeight: 'bold' }}>{G_DadosConfigPdv.FANTASIA === '' ? '-' : G_DadosConfigPdv.FANTASIA}</div>
            </Grid>
            <Grid item xs={3} style={{ textAlign: 'center' }}>
              <div style={{ fontSize: 12, marginBottom: 8 }}>Operador</div>
              <div style={{ fontSize: 14, fontWeight: 'bold' }}>-</div>
            </Grid>
            <Grid item xs={3} style={{ textAlign: 'center' }}>
              <div style={{ fontSize: 12, marginBottom: 8 }}>Vendedor</div>
              <div style={{ fontSize: 14, fontWeight: 'bold' }}>-</div>
            </Grid>
            <Grid item xs={3} style={{ textAlign: 'center' }}>
              <div style={{ fontSize: 12, marginBottom: 8 }}>Data</div>
              <div style={{ fontSize: 14, fontWeight: 'bold' }}>{Format(Date.now(), 'pt-BR', 'America/Sao_Paulo')}</div>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </Grid>
  )
}

export default Status

import React from 'react'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import Dialog from '@material-ui/core/Dialog'
import { DialogContent } from '@material-ui/core'
import { useAppContext } from '../../context/AppContexts'
import { Button } from '@material-ui/core'
import HelpIcon from '@material-ui/icons/Help'
import { useLocation, useHistory } from 'react-router-dom'

interface IQuestion {
  Question: string
  TipoAcao: string
}

export const DialogQuestion = ({ Question, TipoAcao }: IQuestion) => {
  const location = useLocation()
  const history = useHistory()

  const {
    G_DialogQuestion,
    SetDialogQuestion,
    SetListVenda,
    SetStatusVendasAv,
    SetStatusVendasPl,
    SetCpfCnpj,
    SetListOrc,
    SetPagamentos,
    SetResta,
    G_Venda
  } = useAppContext()

  const handleClose = () => {
    SetDialogQuestion(false)
  }

  const handleOk = () => {
    if (TipoAcao === 'Suspender') {
      SetListVenda([])
      SetStatusVendasAv(false)
      SetStatusVendasPl(false)
      SetCpfCnpj('')
      SetPagamentos([])
      SetResta(0)
      SetListOrc([])
      SetDialogQuestion(false)
      G_Venda.ITENS = [];
      G_Venda.PAGAMENTO = [];
      G_Venda.QUANTIDADE_TOTAL = 0;
      //  G_Venda.VALOR_TOTAL_DESCONTO = 0;
      //  G_Venda.VALOR_TOTAL_FRETE = 0;
      //  G_Venda.VALOR_TOTAL_ITENS = 0;
      //  G_Venda.VALOR_TOTAL_VENDA = 0;

      if (location.pathname === '/finalizar') {
        history.push('/')
      }
    }
    SetDialogQuestion(false)
  }
  return (
    <>
      <Dialog aria-labelledby="confirmation-dialog-title" open={G_DialogQuestion}>
        <DialogTitle style={{ width: 500 }} id="confirmation-dialog-title">
          <HelpIcon fontSize="small" /> Pergunta
        </DialogTitle>
        <DialogContent style={{ height: 100 }} dividers>
          <label>
            <strong>{Question}</strong>
          </label>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Não
          </Button>
          <Button onClick={handleOk} color="primary">
            Sim
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default DialogQuestion

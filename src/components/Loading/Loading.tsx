import React, { useEffect, useState } from 'react'
import loading from '../../assets/loading.gif'
import styles from './Loading.module.css'
import { useAppContext } from '../../context/AppContexts'

export function Loading() {
  const { Loading } = useAppContext()
  return (
    <>
      <div className={styles.loading} style={{ display: Loading ? 'flex' : 'none' }}>
        <img src={loading} alt="loading" />
      </div>
    </>
  )
}
export default Loading

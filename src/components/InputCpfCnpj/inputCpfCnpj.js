import * as React from "react";
import { mask as masker, unMask } from "remask";
import TextField from "@material-ui/core/TextField";
import { useAppContext } from "./../../context/AppContexts";

const InputMask = ({ mask, onChange, value, ...props }) => {
  const { SetValidateCpf } = useAppContext();
  const handleChange = ev => {
    const originalValue = unMask(ev.target.value);
    // const maskedValue = masker(originalValue, mask);
    onChange(originalValue);
    SetValidateCpf(false);
  };

  const handleValue = masker(value, mask);

  return <TextField
    {...props}
    style={{ width: "100%" }}
    onChange={handleChange}
    variant="outlined"
    margin="normal"
    type="text"
    placeholder="CPF/CNPJ"
    label="CPF/CNPJ"
    autoFocus
    value={handleValue} />;
};

export const InputCpfCnpj = () => {

  const { SetCpfCnpj, G_CpfCnpj, G_ValidateCpfCnpj } = useAppContext();

  return (
    <>
      <InputMask
        type="text"
        name="doc"
        mask={["999.999.999-99", "99.999.999/9999-99"]}
        error={G_ValidateCpfCnpj}
        helperText={G_ValidateCpfCnpj ? "CPF/CNPJ inválido." : ""}
        onChange={SetCpfCnpj}
        value={G_CpfCnpj}
      />
    </>
  );
}

export default InputCpfCnpj;

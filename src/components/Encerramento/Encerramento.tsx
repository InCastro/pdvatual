import React, { useState } from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button, TextField, IconButton } from '@material-ui/core'
import axios from 'axios'
import { useAppContext } from './../../context/AppContexts'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import Grid from '@material-ui/core/Grid'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import AddIcon from '@material-ui/icons/Add';
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import DeleteIcon from '@material-ui/icons/Delete'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { formatters } from '../../utils'
import Swal from 'sweetalert2'
import { useHistory } from 'react-router-dom'

export function Encerramento() {
    const { OpenDialogEncerramento, SetOpenDialogEncerramento, SetLoading, G_IdTurno } = useAppContext()

    const handleClose = () => {
        SetOpenDialogEncerramento(false)
    }
    const history = useHistory()

    const [result, Setresult] = useState('')
    const [descricaoPagto, setDescricaoPagto] = useState('')
    const [FormaDePagamento, SetFormaDePagamento] = useState(0)


    const methods = [
        {
            id: 0,
            description: 'Dinheiro',
        },
        {
            id: 1,
            description: 'Crédito',
        },
        {
            id: 2,
            description: 'Débito',
        },
        {
            id: 3,
            description: 'Pix',
        },
        {
            id: 4,
            description: 'Boleto',
        },
    ]

    const formatarMoeda = (event: any) => {
        if (result === '') {
            var elemento: string = event.target.value
        } else {
            var elemento: string = event.target.value
        }
        let valor = elemento
        valor = valor + ''
        valor = valor.replace(/[\D]+/g, '')
        valor = valor + ''
        valor = valor.replace(/([0-9]{2})$/g, ',$1')
        if (valor.length > 6) {
            valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2')
        }
        if (valor.length === 3) {
            valor = 0 + valor
        }
        let number = valor.substring(1, 0)
        if (valor.length > 4 && parseInt(number) === 0) {
            valor = valor.substr(1, 14)
        }
        Setresult(`R$ ${valor}`)
    }

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        SetFormaDePagamento(event.target.value as number)
    }

    interface IEncerramento {
        ID_TURNO: number,
        ENCERRAMENTO: Array<IEncerramento_Detalhe>
    }

    interface IEncerramento_Detalhe {
        TIPO_PAGTO: string,
        VALOR: number
    }

    const initialEncerramento: IEncerramento = {
        ID_TURNO: 0,
        ENCERRAMENTO: []
    }

    const [encerramentoCaixa, setEncerramentoCaixa] = useState<IEncerramento>(initialEncerramento)


    const AddEncerramento = () => {
        encerramentoCaixa.ID_TURNO = G_IdTurno;
        encerramentoCaixa.ENCERRAMENTO.push({
            TIPO_PAGTO: methods[FormaDePagamento].description,
            VALOR: parseFloat(result.replace('R$ ', '').replace('R$ ', '').replace('.', '').replace(',', '.'))
        })
        Setresult(`R$ 0,00`)
    }

    const removerPagto = (index: number) => {
        SetLoading(true)
        setTimeout(() => {
            encerramentoCaixa.ENCERRAMENTO.splice(index, 1)
            SetLoading(false)
        }, 1000)

    }

    const postEncerramento = () => {
        SetLoading(true)
        const method = 'post';
        const url = `http://localhost:3333/encerrarCaixa`;
        axios[method](url, encerramentoCaixa)
            .then((res) => {
                setTimeout(() => {
                    const method2 = 'get';
                    const url2 = `http://localhost:3333/encerrarCaixa/${G_IdTurno}`;
                    axios[method2](url2)
                        .then((res) => {
                            Swal.fire({
                                icon: 'success',
                                text: 'Encerramento de caixa realizado com sucesso.',
                            })
                            SetOpenDialogEncerramento(false)
                            SetLoading(false)
                            history.push('./login')
                        })
                }, 1000)

            })


    }

    return (
        <div>
            <Dialog open={OpenDialogEncerramento} aria-labelledby="form-dialog-title">
                <DialogTitle>
                    ENCERRAMENTO DE CAIXA
                </DialogTitle>
                <DialogContent style={{ height: 500, width: 600 }} dividers>
                    <Grid item xs>
                        <FormControl variant="outlined" style={{ width: '42%' }}>
                            <InputLabel>Forma de Pagamento</InputLabel>
                            <Select value={FormaDePagamento} autoFocus onChange={handleChange} label="Forma de Pagamento">
                                {methods.map((method) => (
                                    <MenuItem value={method.id}>{method.description}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <TextField
                            style={{ marginLeft: 10, width: '40%' }}
                            id="outlined-size-normal"
                            variant="outlined"
                            placeholder="Valor R$"
                            onChange={formatarMoeda}
                            type="text"
                            inputProps={{
                                maxlength: 13,
                            }}
                            value={result}
                        />
                        <IconButton style={{ backgroundColor: '#6022a9', color: '#FFF', marginLeft: 10 }} onClick={AddEncerramento} aria-label="add">
                            <AddIcon fontSize="large" />
                        </IconButton>
                    </Grid>
                    <br />
                    <br />
                    <Table stickyHeader>
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">DESCRIÇÃO</TableCell>
                                <TableCell align="right">VALOR </TableCell>
                                <TableCell align="center"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {encerramentoCaixa.ENCERRAMENTO.map((enc, index) => (
                                <TableRow>
                                    <TableCell align="left" component="th" scope="row">
                                        {enc.TIPO_PAGTO}
                                    </TableCell>
                                    <TableCell align="right" component="th" scope="row">
                                        {formatters.formatMoney(enc.VALOR)}
                                    </TableCell>
                                    <TableCell align="right" component="th" scope="row">
                                        {<IconButton style={{ backgroundColor: '#6022a9', color: '#FFF', marginLeft: 10 }} onClick={() => removerPagto(index)} aria-label="add">
                                            <DeleteIcon />
                                        </IconButton>}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>

                </DialogContent>
                <DialogActions>
                    <Button id={'buttonCancelar'} onClick={handleClose} color="primary">
                        Cancelar (ESC)
                    </Button>
                    <Button id={'buttonCancelar'} onClick={postEncerramento} color="primary">
                        Salvar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default Encerramento;
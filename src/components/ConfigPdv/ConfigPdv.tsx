import React, { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import styles from './Config.module.css'
import BuildIcon from '@material-ui/icons/Build';
import { TextField, Button } from '@material-ui/core';
import { InputCpfCnpj } from './../InputCpfCnpj';
import { useHistory } from 'react-router-dom'
import axios from 'axios';
import { UrlAPI } from '../../utils';
import { useAppContext } from './../../context/AppContexts'
import Swal from 'sweetalert2'

export function ConfigPDV() {
    const regTributario = [
        {
            value: '0',
            label: 'Lucro Presumido',
        },
        {
            value: '1',
            label: 'Lucro Real',
        },
        {
            value: '2',
            label: 'Simples Nacional',
        }
    ];


    const AmbienteSat = [
        {
            value: '0',
            label: 'Homologação',
        },
        {
            value: '1',
            label: 'Produção',
        }
    ];

    const { G_ConfigInitial, SetLoading, G_DadosConfigPdv, SetGDadosPdv, SetCpfCnpj, G_CpfCnpj } = useAppContext()
    const [valorAlerta, SetValorAlerta] = useState('')

    const history = useHistory()

    useEffect(() => {
        SetCpfCnpj(G_DadosConfigPdv.CNPJ_EMPRESA)
        SetValorAlerta(`R$ ${G_DadosConfigPdv.VALOR_SANGRIA},00`)
        setTimeout(() => {
            SetLoading(false)
        }, 1000)
    })

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
    })

    const SaveConfig = () => {
        swalWithBootstrapButtons.fire({
            title: 'Salvar Configurações?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                SetLoading(true)
                G_DadosConfigPdv.CNPJ_EMPRESA = G_CpfCnpj;
                const method = G_ConfigInitial ? 'post' : 'put';
                const url = `${UrlAPI}salvarParametros`;
                axios[method](url, G_DadosConfigPdv)
                    .then((response) => {
                        SetCpfCnpj('')
                        setTimeout(() => {
                            SetLoading(false)
                            swalWithBootstrapButtons.fire(
                                'Salvo!',
                                'Configurações salvas com sucesso.',
                                'success'
                            )
                        }, 1000)
                        if (G_ConfigInitial) {
                            history.push('./login')
                        } else {
                            history.push('./')
                        }

                    })
                    .catch((error) => {
                        setTimeout(() => {
                            SetLoading(false)
                            swalWithBootstrapButtons.fire(
                                'Erro!',
                                `Erro ao salvar as configurações do PDV. ${error}`,
                                'error'
                            )
                        }, 1000)
                    })

            }
        })
    }

    const handleChangeRegTribut = (e: any) => {
        SetGDadosPdv({ ...G_DadosConfigPdv, REGIME_TRIBUTARIO: e.target.value })
    }

    const handleChangeAmbiente = (e: any) => {
        SetGDadosPdv({ ...G_DadosConfigPdv, AMBIENTE_SAT: e.target.value })
    }

    const CancelarConfig = () => {
        history.push('./')
    }

    const formatarMoeda = (event: any) => {
        let valor: string = event.target.value
        valor = valor + ''
        valor = valor.replace(/[\D]+/g, '')
        valor = valor + ''
        valor = valor.replace(/([0-9]{2})$/g, ',$1')
        if (valor.length > 6) {
            valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2')
        }
        if (valor.length === 3) {
            valor = 0 + valor
        }
        let number = valor.substring(1, 0)
        if (valor.length > 4 && parseInt(number) === 0) {
            valor = valor.substr(1, 14)
        }
        SetValorAlerta(`R$ ${valor}`);
        SetGDadosPdv({ ...G_DadosConfigPdv, VALOR_SANGRIA: parseFloat(valor.replace('.', '').replace(',', '.')) })
    }

    return (
        <div style={{ flexGrow: 1 }}>
            <Grid>
                <Grid item xs={12} className={styles.Grid}>
                    <Card style={{ backgroundColor: '#FAFAFA' }} className={styles.Card1} variant="outlined" square>
                        <br />
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <BuildIcon style={{ marginLeft: 10 }} />
                                <label style={{ marginLeft: 10 }}> <strong> Configuração PDV</strong> </label>
                            </Grid>
                            <Grid item xs={2} style={{ marginLeft: 20 }}>
                                <InputCpfCnpj />
                            </Grid>
                            <Grid item xs={2} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Inscrição Estadual"
                                    label="Inscrição Estadual"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, IE: e.target.value })}
                                    value={G_DadosConfigPdv.IE}
                                />
                            </Grid>
                            <Grid item xs={3} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Razão Social"
                                    label="Razão Social"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, RAZAO_SOCIAL: e.target.value })}
                                    value={G_DadosConfigPdv.RAZAO_SOCIAL}
                                />
                            </Grid>

                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs={3} style={{ marginLeft: 20 }}>
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Fantasia"
                                    label="Fantasia"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, FANTASIA: e.target.value })}
                                    value={G_DadosConfigPdv.FANTASIA}
                                />
                            </Grid>
                            <Grid item xs={4} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Endereço"
                                    label="Endereço"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, ENDERECO_EMPRESA: e.target.value })}
                                    value={G_DadosConfigPdv.ENDERECO_EMPRESA}
                                />
                            </Grid>

                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs={1} style={{ marginLeft: 20 }} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="CEP"
                                    label="CEP"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, CEP_EMPRESA: e.target.value })}
                                    value={G_DadosConfigPdv.CEP_EMPRESA}
                                />
                            </Grid>
                            <Grid item xs={2} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Fone"
                                    label="Fone"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, FONE_EMPRESA: e.target.value })}
                                    value={G_DadosConfigPdv.FONE_EMPRESA}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs={1} style={{ marginLeft: 20 }}>
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Núm. Caixa"
                                    label="Núm. Caixa"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, NUM_CAIXA: e.target.value })}
                                    value={G_DadosConfigPdv.NUM_CAIXA}
                                />
                            </Grid>
                            <Grid item xs={2} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="R$ Valor Sangria"
                                    label="R$ Valor alerta Sangria"
                                    onChange={formatarMoeda}
                                    value={valorAlerta}
                                />
                            </Grid>
                            <Grid item xs={2} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Régime Tributário"
                                    label="Régime Tributário"
                                    select
                                    onChange={handleChangeRegTribut}
                                    SelectProps={{
                                        native: true,
                                    }}
                                >
                                    {regTributario.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </TextField>
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs={3} style={{ marginLeft: 20 }}>
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Cód. Ativação SAT"
                                    label="Cód. Ativação SAT"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, COD_ATVSAT: e.target.value })}
                                    value={G_DadosConfigPdv.COD_ATVSAT}
                                />
                            </Grid>
                            <Grid item xs={2} >
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    placeholder="Ambiente SAT"
                                    label="Ambiente SAT"
                                    select
                                    onChange={handleChangeAmbiente}
                                    SelectProps={{
                                        native: true,
                                    }}
                                >
                                    {AmbienteSat.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </TextField>
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs={7} style={{ marginLeft: 20 }}>
                                <TextField
                                    style={{ width: "100%" }}
                                    variant="outlined"
                                    margin="normal"
                                    type="text"
                                    multiline
                                    rows={6}
                                    placeholder="Assinatura SAT"
                                    label="Assinatura SAT"
                                    onChange={(e) => SetGDadosPdv({ ...G_DadosConfigPdv, ASS_SAT: e.target.value })}
                                    value={G_DadosConfigPdv.ASS_SAT}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs={1} style={{ marginLeft: 20 }}>
                                <Button style={{ backgroundColor: '#6022a9', color: '#FFF', width: '100%' }} id={'buttonSalvar'} onClick={SaveConfig} color="primary">
                                    Salvar
                                </Button>
                            </Grid>
                            <Grid item xs={1}>
                                <Button style={{ backgroundColor: '#6022a9', color: '#FFF', width: '100%' }} id={'buttonCancelar'} onClick={CancelarConfig} color="primary">
                                    Cancelar
                                </Button>
                            </Grid>
                        </Grid>
                    </Card>
                </Grid>
            </Grid>
        </div>
    )
}

export default ConfigPDV;
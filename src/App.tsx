import React, { lazy, Suspense } from 'react'
import { Switch, Route, HashRouter } from 'react-router-dom'
import { AppContextProvider } from './context/AppContexts'
import { Header } from './components/Header'
import { Status } from './components/Status'
import { Loading } from './components/Loading'
import './i18n/config'
import { Login } from './components/Login'

const Pagamento = lazy(() => import('./components/Pagamento/Pagamento'))
const TheTelaPrincipal = lazy(() => import('./components/Principal/Principal'))
const ConfigPdv = lazy(() => import('./components/ConfigPdv/ConfigPdv'))
export default function App() {
  return (
    <>
      <AppContextProvider>
        <HashRouter>
          <Suspense fallback>
            <Loading />
            <Header />
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/" exact component={TheTelaPrincipal} />
              <Route path="/finalizar" component={Pagamento} />
              <Route path="/configPdv" component={ConfigPdv} />
            </Switch>
            <Status />
          </Suspense>
        </HashRouter>
      </AppContextProvider>
    </>
  )
}
